#![feature(let_chains)]
#![feature(trait_alias)]

pub mod bes;
pub mod calculus;
pub mod error;
pub mod interner;
pub mod lts;
pub mod parity;
#[cfg(test)]
mod test;

pub use bes::{Bes, Srf};
pub use calculus::Formula;
pub use error::ParseError;
pub use lts::Lts;

use interner::StrId;
use roaring::RoaringBitmap;
use rustc_hash::FxHashMap as HashMap;

pub type State = u32;
pub type Label = StrId;
pub type StateSet = RoaringBitmap;

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum Fixpoint {
	Mu,
	Nu,
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum Logic {
	And,
	Or,
}

#[macro_export]
macro_rules! max {
	($x:expr, $($xs:expr),*) => {
		$x $(.max($xs))*
	}
}

#[macro_export]
macro_rules! rb {
	($($key:expr,)+) => (bitmap!($($key),+));
	( $($key:expr),* ) => {
		{
			let mut _bm = ::roaring::RoaringBitmap::new();
			$(
				_bm.insert($key);
			)*
			_bm
		}
	};
}
