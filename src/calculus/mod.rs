mod basic;
mod emerson_lei;
pub mod parity;
pub mod parser;

pub use crate::{error::McfParseError as ParseError, Fixpoint as FpKind, Label, Logic as BinOp};
pub use basic::eval as basic;
pub use emerson_lei::eval as emerson_lei;
pub use parity::eval as parity;

use crate::max;
use parser::{Cst, Spanned};
use std::{
	collections::{BTreeSet, VecDeque},
	fmt,
	str::FromStr,
};
use tracing::trace;

pub type VarName = char;

#[derive(Clone, Eq, PartialEq)]
pub enum Formula {
	False,
	True,
	Var {
		name: VarName,
	},
	Binary {
		op: BinOp,
		f1: Box<Formula>,
		f2: Box<Formula>,
	},
	Modal {
		op: ModalKind,
		step: Label,
		f: Box<Formula>,
	},
	Fixpoint {
		kind: FpKind,
		var: VarName,
		f: Box<Formula>,
	},
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum ModalKind {
	Box,
	Diamond,
}

impl Formula {
	pub fn subformulas(&self) -> impl Iterator<Item = &Self> {
		use Formula::*;
		let mut frontier = VecDeque::from([self]);
		std::iter::from_fn(move || {
			frontier.pop_front().map(|f| {
				match &f {
					False | True | Var { .. } => (),
					Binary { f1, f2, .. } => frontier.extend([&**f1, &**f2]),
					Modal { f, .. } => frontier.push_back(f),
					Fixpoint { f, .. } => frontier.push_back(f),
				};
				f
			})
		})
	}

	pub fn actions(&self) -> impl Iterator<Item = Label> + '_ {
		use Formula::*;
		self.subformulas()
			.filter_map(|f| match f {
				Modal { step, .. } => Some(step),
				_ => None,
			})
			.copied()
	}

	#[must_use]
	pub fn is_open(&self) -> bool {
		let vars = self.variables();
		!vars.used.is_subset(&vars.declared)
	}

	#[must_use]
	pub fn nesting_depth(&self) -> u16 {
		use Formula::*;
		match self {
			True | False | Var { .. } => 0,
			Modal { f, .. } => f.nesting_depth(),
			Binary { f1, f2, .. } => max!(f1.nesting_depth(), f2.nesting_depth()),
			Fixpoint { f, .. } => 1 + f.nesting_depth(),
		}
	}

	#[must_use]
	pub fn alternation_depth(&self) -> u16 {
		use Formula::*;
		match self {
			True | False | Var { .. } => 0,
			Modal { f, .. } => f.alternation_depth(),
			Binary { f1, f2, .. } => max!(f1.alternation_depth(), f2.alternation_depth()),
			Fixpoint { kind, f, .. } => {
				max!(
					1,
					f.alternation_depth(),
					1 + f
						.subformulas()
						.filter(|g| kind.is_alternate(g))
						.map(Formula::alternation_depth)
						.max()
						.unwrap_or(0)
				)
			},
		}
	}

	#[must_use]
	pub fn dependent_ad(&self) -> u16 {
		use Formula::*;
		match self {
			True | False | Var { .. } => 0,
			Modal { f, .. } => f.dependent_ad(),
			Binary { f1, f2, .. } => max!(f1.dependent_ad(), f2.dependent_ad()),
			Fixpoint { kind, var, f } => {
				max!(
					1,
					f.dependent_ad(),
					1 + f
						.subformulas()
						.filter(|g| kind.is_alternate(g) && g.variables().used.contains(var))
						.map(Formula::dependent_ad)
						.max()
						.unwrap_or(0)
				)
			},
		}
	}

	pub(crate) fn is_mu(&self) -> bool {
		matches!(
			self,
			Formula::Fixpoint {
				kind: FpKind::Mu,
				..
			}
		)
	}

	pub(crate) fn is_nu(&self) -> bool {
		matches!(
			self,
			Formula::Fixpoint {
				kind: FpKind::Nu,
				..
			}
		)
	}

	fn variables(&self) -> Variables {
		use Formula::*;
		let mut vars = Variables::default();
		match self {
			Var { name } => {
				vars.used.insert(*name);
			},
			Binary { f1, f2, .. } => {
				vars = f1.variables();
				vars.union(f2.variables());
			},
			Modal { f, .. } => vars = f.variables(),
			Fixpoint { var, f, .. } => {
				vars = f.variables();
				vars.declared.insert(*var);
			},
			_ => (),
		}
		vars
	}
}

impl FpKind {
	pub(crate) fn is_alternate(self, g: &Formula) -> bool {
		use FpKind::*;
		match self {
			Mu if g.is_nu() => true,
			Nu if g.is_mu() => true,
			_ => false,
		}
	}
}

#[derive(Clone, Debug, Default)]
struct Variables {
	declared: BTreeSet<VarName>,
	used: BTreeSet<VarName>,
}

impl Variables {
	fn union(&mut self, mut other: Self) {
		self.declared.append(&mut other.declared);
		self.used.append(&mut other.used);
	}
}

pub fn from_str(s: &str) -> Result<Spanned<Cst>, Vec<chumsky::error::Simple<String>>> {
	use chumsky::{Parser, Stream};
	use logos::Logos;

	let lex = parser::Token::lexer(s);
	let len = lex.source().len();
	let eoi = len..len + 1;
	let stream = Stream::from_iter(eoi, lex.spanned());
	// let (ast, errs) = parser::parser().parse_recovery(stream);
	// if !errs.is_empty() {
	// 	dbg!(ast, errs);
	// 	panic!("asdf")
	// }
	let result = parser::parser()
		.parse(stream)
		.map_err(|errs| errs.into_iter().map(|e| e.map(|t| t.to_string())).collect());
	trace!("{:?}", result);
	result
}

impl FromStr for Formula {
	type Err = Vec<chumsky::error::Simple<String>>;

	fn from_str(s: &str) -> Result<Self, Self::Err> {
		from_str(s).map(Into::into)
	}
}

impl fmt::Display for Formula {
	fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
		use self::{BinOp::*, Formula::*, FpKind::*, ModalKind::*};
		match self {
			False => write!(fmt, "false"),
			True => write!(fmt, "true"),
			Var { name } => write!(fmt, "{}", name),
			Binary { op: And, f1, f2 } => write!(fmt, "({} && {})", f1, f2),
			Binary { op: Or, f1, f2 } => write!(fmt, "({} || {})", f1, f2),
			Modal {
				op: Diamond,
				step,
				f,
			} => write!(fmt, "<{}>{}", step, f),
			Modal { op: Box, step, f } => write!(fmt, "[{}]{}", step, f),
			Fixpoint { kind: Mu, var, f } => write!(fmt, "mu {}. ({})", var, f),
			Fixpoint { kind: Nu, var, f } => write!(fmt, "nu {}. ({})", var, f),
		}
	}
}

impl fmt::Debug for Formula {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "{}", self)
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use crate::interner::intern;

	#[test]
	fn literals() {
		let f = "false".parse::<Formula>().unwrap();
		assert_eq!(f, Formula::False);

		let f = "true".parse::<Formula>().unwrap();
		assert_eq!(f, Formula::True);
	}

	#[test]
	fn binary_operators() {
		use self::{BinOp::*, Formula::*};
		let f = "(false &&  true)".parse::<Formula>().unwrap();
		assert_eq!(
			f,
			Binary {
				op: And,
				f1: Box::new(False),
				f2: Box::new(True)
			}
		);

		let f = "( false || (true &&true))".parse::<Formula>().unwrap();
		assert_eq!(
			f,
			Binary {
				op: Or,
				f1: Box::new(False),
				f2: Box::new(Binary {
					op: And,
					f1: Box::new(Formula::True),
					f2: Box::new(Formula::True),
				}),
			},
		);

		let f = "( ( false || false) && (true|| false))"
			.parse::<Formula>()
			.unwrap();
		assert_eq!(
			f,
			Binary {
				op: And,
				f1: Box::new(Binary {
					op: Or,
					f1: Box::new(Formula::False),
					f2: Box::new(Formula::False),
				}),
				f2: Box::new(Binary {
					op: Or,
					f1: Box::new(Formula::True),
					f2: Box::new(Formula::False),
				}),
			},
		);
	}

	#[test]
	fn modal_operators() {
		use self::{Formula::*, ModalKind::Diamond};

		let f = "[tau]true".parse::<Formula>().unwrap();
		assert_eq!(
			f,
			Modal {
				op: ModalKind::Box,
				step: intern("tau"),
				f: Box::new(Formula::True)
			}
		);

		let f = "<tau>false".parse::<Formula>().unwrap();
		assert_eq!(
			f,
			Modal {
				op: Diamond,
				step: intern("tau"),
				f: Box::new(Formula::False)
			}
		);

		let f = "[tau]<tau>true".parse::<Formula>().unwrap();
		assert_eq!(
			f,
			Modal {
				op: ModalKind::Box,
				step: intern("tau"),
				f: Box::new(Modal {
					op: Diamond,
					step: intern("tau"),
					f: Box::new(Formula::True)
				}),
			}
		);

		let f = "<tau>[tau]false".parse::<Formula>().unwrap();
		assert_eq!(
			f,
			Modal {
				op: Diamond,
				step: intern("tau"),
				f: Box::new(Modal {
					op: ModalKind::Box,
					step: intern("tau"),
					f: Box::new(Formula::False)
				}),
			}
		);
	}

	#[test]
	fn fixpoints() {
		use self::{BinOp::*, Formula::*, FpKind::*, ModalKind::Diamond};
		let f = "mu X. X".parse::<Formula>().unwrap();
		assert_eq!(
			f,
			Fixpoint {
				kind: Mu,
				var: 'X',
				f: Box::new(Formula::Var { name: 'X' })
			}
		);

		let f = "nu Y. Y".parse::<Formula>().unwrap();
		assert_eq!(
			f,
			Fixpoint {
				kind: Nu,
				var: 'Y',
				f: Box::new(Formula::Var { name: 'Y' })
			}
		);

		let f = "mu X. <tau>X".parse::<Formula>().unwrap();
		assert_eq!(
			f,
			Fixpoint {
				kind: Mu,
				var: 'X',
				f: Box::new(Modal {
					op: Diamond,
					step: intern("tau"),
					f: Box::new(Formula::Var { name: 'X' })
				}),
			}
		);

		let f = "mu X. nu Y. (X || Y)".parse::<Formula>().unwrap();
		assert_eq!(
			f,
			Fixpoint {
				kind: Mu,
				var: 'X',
				f: Box::new(Fixpoint {
					kind: Nu,
					var: 'Y',
					f: Box::new(Binary {
						op: Or,
						f1: Box::new(Formula::Var { name: 'X' }),
						f2: Box::new(Formula::Var { name: 'Y' })
					})
				}),
			}
		);

		let f = "nu X. (X && mu Y. Y)".parse::<Formula>().unwrap();
		assert_eq!(
			f,
			Fixpoint {
				kind: Nu,
				var: 'X',
				f: Box::new(Binary {
					op: And,
					f1: Box::new(Formula::Var { name: 'X' }),
					f2: Box::new(Fixpoint {
						kind: Mu,
						var: 'Y',
						f: Box::new(Formula::Var { name: 'Y' })
					})
				})
			}
		);
	}

	#[test]
	fn depth_measures() {
		let f = "(mu X.nu Y.X||Y)&& mu V. mu W. V && mu Z.true || Z"
			.parse::<Formula>()
			.unwrap();
		assert_eq!(f.nesting_depth(), 3);
		assert_eq!(f.alternation_depth(), 2);
		assert_eq!(f.dependent_ad(), 2);
	}
}
