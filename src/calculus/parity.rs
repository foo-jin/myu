use crate::{
	bes::{self, srf, Bes, VarState},
	calculus::{Formula, VarName},
	lts::Lts,
	parity::{zielonka, Game, Graph, Player::*, Vertex},
	HashMap, StateSet,
};
use petgraph::{graph::NodeIndex, stable_graph::StableGraph};

#[must_use]
pub fn eval(lts: &Lts, f: &Formula) -> StateSet {
	let bes = Bes::try_from((lts, f)).unwrap();
	let srf = bes.to_srf();
	let mut parity = Mapped::from(&srf);
	parity.0.simplify();
	parity.eval()
}

type Var = srf::Var<VarState>;

#[derive(Debug)]
pub struct Mapped(Game, Option<VarName>, HashMap<NodeIndex, Var>);

impl Mapped {
	#[must_use]
	pub fn graph(&self) -> StableGraph<String, ()> {
		let delimit = |v: &Vertex| match v.owner {
			Even => ['<', '>'],
			Odd => ['[', ']'],
		};
		self.0.graph().map(
			|ix, v| {
				format!(
					"{}{:?}: {}{}",
					delimit(v)[0],
					self.2[&ix],
					v.priority,
					delimit(v)[1]
				)
			},
			|_, _| (),
		)
	}

	#[must_use]
	pub fn eval(&self) -> StateSet {
		let Mapped(game, var, mapping) = self;
		let var = match var {
			Some(v) => v,
			None => return StateSet::default(),
		};

		let part = zielonka(game);
		let even = &part[Even];

		even.iter()
			.filter_map(|s| mapping.get(&NodeIndex::new(s as usize)))
			.filter_map(|((v, s), trail)| (v == var && *trail == 0).then_some(s))
			.collect()
	}
}

impl From<&bes::Srf<Var>> for Mapped {
	fn from(srf: &bes::Srf<Var>) -> Self {
		let mut g = Graph::new();
		let mut mapping = HashMap::default();
		for (rank, eq) in srf.equations() {
			let owner = if eq.is_conjunction() { Odd } else { Even };
			let vertex = Vertex {
				owner,
				priority: rank,
			};
			let node = g.add_node(vertex);
			mapping.insert(eq.var(), node);
		}

		for (s, t) in srf
			.equations()
			.flat_map(|(_, eq)| eq.variables().map(|v| (eq.var(), v)))
		{
			let (s, t) = (mapping[&s], mapping[&t]);
			g.add_edge(s, t, ());
		}

		let mapping = mapping.into_iter().map(|(k, v)| (v, k)).collect();
		let var = srf.equations().next().map(|(_, eq)| eq.var().0 .0);
		Mapped(Game::new(g), var, mapping)
	}
}
