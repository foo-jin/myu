use super::{
	emerson_lei::{self, Env, Eval, Variables},
	Formula,
};
use crate::lts::Lts;

#[must_use]
pub fn eval(lts: &Lts, f: &Formula) -> Eval {
	let mut vars = Variables::default();
	emerson_lei::eval_inner(
		lts,
		f,
		&mut Env {
			prev_fp: None,
			vars: &mut vars,
			reset: false,
		},
	)
}
