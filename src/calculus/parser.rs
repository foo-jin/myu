use super::{
	BinOp::{self, *},
	Formula,
	FpKind::{self, *},
	Label,
	ModalKind::{self, Diamond},
	VarName,
};
use crate::interner::{self, StrId};
use chumsky::prelude::*;
use logos::Logos;
use std::{collections::VecDeque, fmt, ops::Range};

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Spanned<T>(pub T, pub Range<usize>);

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Cst {
	False,
	True,
	Var {
		name: VarName,
	},
	Binary {
		op: BinOp,
		f1: Box<Spanned<Cst>>,
		f2: Box<Spanned<Cst>>,
	},
	Modal {
		op: ModalKind,
		step: Label,
		f: Box<Spanned<Cst>>,
	},
	Fixpoint {
		kind: FpKind,
		var: VarName,
		f: Box<Spanned<Cst>>,
	},
}

impl Spanned<Cst> {
	pub fn subformulas(&self) -> impl Iterator<Item = &Self> {
		use Cst::*;
		let mut frontier = VecDeque::from([self]);
		std::iter::from_fn(move || {
			frontier.pop_front().map(|f| {
				match &f.0 {
					False | True | Var { .. } => (),
					Binary { f1, f2, .. } => frontier.extend([&**f1, &**f2]),
					Modal { f, .. } | Fixpoint { f, .. } => frontier.push_back(f),
				};
				f
			})
		})
	}
}

#[rustfmt::skip]
#[derive(Logos, Copy, Clone, Debug, Hash, Eq, PartialEq)]
pub(super) enum Token {
	#[token("true")] True,
	#[token("false")] False,
	#[regex("[A-Z]", |lex| lex.slice().chars().next())]
	Var(char),
	#[token("(")] LParen,
	#[token(")")] RParen,
	#[token("||")] Or,
	#[token("&&")] And,
	#[token("<")] LDiamond,
	#[token(">")] RDiamond,
	#[token("[")] LBox,
	#[token("]")] RBox,
	#[regex("[a-z][a-z0-9_]*", |lex| interner::intern(lex.slice()))]
	Action(StrId),
	#[token("mu")] Mu,
	#[token("nu")] Nu,
	#[token(".")] Period,
	#[regex(r"%[^\n]*\n", logos::skip)] Comment,
	#[regex(r"[ \t\n\f]+")] WS,
	#[error] Error,
}

pub(super) fn parser() -> impl Parser<Token, Spanned<Cst>, Error = Simple<Token>> {
	use Cst::*;
	recursive(|formula| {
		let ws = just(Token::WS).repeated();

		let atom = {
			let (lparen, rparen) = (just(Token::LParen), just(Token::RParen));
			let literal = select! {
				Token::True => True,
				Token::False => False,
				Token::Var(c) => Var { name: c },
			}
			.map_with_span(Spanned);

			let parens = formula.clone().delimited_by(lparen, rparen);
			literal.or(parens)
		};

		let fixpoint = {
			let fp = select! {
				Token::Mu => Mu,
				Token::Nu => Nu,
			};
			let var = select! { Token::Var(c) => c };
			let period = just(Token::Period);

			fp.then_ignore(ws.at_least(1))
				.then(var)
				.then_ignore(period)
				.then(formula.clone())
				.map(move |((kind, var), g)| Fixpoint {
					kind,
					var,
					f: Box::new(g),
				})
				.map_with_span(Spanned)
		};

		let modal = recursive(|modal| {
			let action = select! { Token::Action(id) => id };
			let unambiguous = choice((atom.clone(), fixpoint.clone(), modal)).padded_by(ws);

			let modal = |kind: ModalKind| {
				let (open, close) = kind.lit();
				action
					.delimited_by(just(open), just(close))
					.then(unambiguous.clone())
					.map(move |(step, f)| Modal {
						op: kind,
						step,
						f: Box::new(f),
					})
			};

			modal(ModalKind::Diamond)
				.or(modal(ModalKind::Box))
				.map_with_span(Spanned)
		});

		let logical = {
			let op_lit = select! {
				Token::And => And,
				Token::Or => Or,
			};
			let higher_precedence = choice((atom.clone(), modal.clone())).padded_by(ws);

			// only accept operators that bind more tightly on the left side
			higher_precedence
				.then(op_lit.then(formula).repeated().at_least(1))
				.foldl(move |lhs, (op, rhs)| {
					let span = lhs.1.start..rhs.1.end;
					Spanned(
						Binary {
							op,
							f1: Box::new(lhs),
							f2: Box::new(rhs),
						},
						span,
					)
				})
		};

		choice((fixpoint, logical, modal, atom)).padded_by(ws)
	})
	.then_ignore(end())
}

impl From<Spanned<Cst>> for Formula {
	fn from(Spanned(cst, _): Spanned<Cst>) -> Self {
		match cst {
			Cst::False => Formula::False,
			Cst::True => Formula::True,
			Cst::Var { name } => Formula::Var { name },
			Cst::Binary { op, f1, f2 } => Formula::Binary {
				op,
				f1: Box::new((*f1).into()),
				f2: Box::new((*f2).into()),
			},
			Cst::Modal { op, step, f } => Formula::Modal {
				op,
				step,
				f: Box::new((*f).into()),
			},
			Cst::Fixpoint { kind, var, f } => Formula::Fixpoint {
				kind,
				var,
				f: Box::new((*f).into()),
			},
		}
	}
}

trait Lit {
	fn lit(&self) -> (Token, Token);
}

impl Lit for ModalKind {
	fn lit(&self) -> (Token, Token) {
		use Token::{LBox, LDiamond, RBox, RDiamond};
		match self {
			ModalKind::Box => (LBox, RBox),
			Diamond => (LDiamond, RDiamond),
		}
	}
}

impl fmt::Display for Token {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		match self {
			Token::True => write!(f, "true"),
			Token::False => write!(f, "false"),
			Token::Var(c) => write!(f, "{c}"),
			Token::LParen => write!(f, "("),
			Token::RParen => write!(f, ")"),
			Token::Or => write!(f, "||"),
			Token::And => write!(f, "&&"),
			Token::LDiamond => write!(f, "<"),
			Token::RDiamond => write!(f, ">"),
			Token::LBox => write!(f, "["),
			Token::RBox => write!(f, "]"),
			Token::Action(id) => write!(f, "{}", interner::lookup(*id)),
			Token::Mu => write!(f, "mu"),
			Token::Nu => write!(f, "nu"),
			Token::Period => write!(f, "."),
			Token::Comment => write!(f, "% ..."),
			Token::WS => write!(f, "WHITESPACE"),
			Token::Error => write!(f, "ERROR"),
		}
	}
}
