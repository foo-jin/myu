//! Implementation of the Emerson-Lei algorithm for evaluating modal μ-calculus formulae.

use crate::{
	calculus::{self as mc, Formula, VarName},
	lts::Lts,
	StateSet,
};
use std::{collections::BTreeMap, mem};
use tracing::{debug, trace};

pub type Eval = (StateSet, u32);
pub type Variables = BTreeMap<VarName, StateSet>;

#[tracing::instrument(skip(lts))]
pub fn eval(lts: &Lts, f: &Formula) -> Eval {
	let mut env = BTreeMap::new();
	for g in f.subformulas() {
		if let Formula::Fixpoint { kind, var, .. } = g {
			env.insert(*var, fp_init_set(*kind, lts));
		}
	}
	eval_inner(
		lts,
		f,
		&mut Env {
			prev_fp: None,
			vars: &mut env,
			reset: true,
		},
	)
}

#[derive(Debug)]
pub struct Env<'a> {
	pub prev_fp: Option<&'a Formula>,
	pub vars: &'a mut Variables,
	pub reset: bool,
}

#[tracing::instrument(skip(lts, env))]
pub fn eval_inner<'a>(lts: &Lts, f: &'a Formula, env: &mut Env<'a>) -> Eval {
	use itertools::Itertools;
	use mc::{BinOp::*, Formula::*, ModalKind::*};

	let result = match f {
		Var { name } => (env.vars[name].clone(), 0),
		True => (lts.states().clone(), 0),
		False => (StateSet::default(), 0),
		Binary { op, f1, f2 } => {
			let (r1, i) = eval_inner(lts, f1, env);
			let (r2, j) = eval_inner(lts, f2, env);
			let result = match op {
				And => r1 & r2,
				Or => r1 | r2,
			};
			(result, i + j)
		},
		Modal { op, step, f: g } => {
			let (r1, i) = eval_inner(lts, g, env);
			let mut result = modal_init_set(*op, lts);
			for (s, mut ts) in &lts.step_transitions(*step).group_by(|&(s, _)| s) {
				match op {
					Diamond if ts.any(|e| r1.contains(e.1)) => {
						result.insert(s);
					},
					Box if !ts.all(|e| r1.contains(e.1)) => {
						result.remove(s);
					},
					_ => (),
				}
			}

			(result, i)
		},
		Fixpoint { kind, var, f: g } if !env.reset => {
			let var_init = fp_init_set(*kind, lts);
			let _ = env.vars.insert(*var, var_init);
			eval_fixpoint(lts, g, *var, env)
		},
		Fixpoint { kind, var, f: g } => {
			if let Some(prev) = env.prev_fp && kind.is_alternate(prev) {
				for g in f.subformulas().filter(|g| g.is_open()){
					if let Fixpoint { kind: gk, var, .. } = g && gk == kind {
						debug!("resetting '{var}'");
						env.vars.insert(*var, fp_init_set(*kind, lts));
					}
				}
			}
			let prev = mem::replace(&mut env.prev_fp, Some(f));
			let result = eval_fixpoint(lts, g, *var, env);
			env.prev_fp = prev;
			result
		},
	};
	trace!("result: {:?}", result);
	result
}

fn eval_fixpoint<'a>(lts: &Lts, g: &'a Formula, var: VarName, env: &mut Env<'a>) -> Eval {
	let mut i = 0;
	loop {
		tracing::trace!("start loop");
		let (new, j) = eval_inner(lts, g, env);
		i += 1 + j;
		let prev = env
			.vars
			.insert(var, new)
			.expect("only pre-set variables should be re-set");
		if prev == env.vars[&var] {
			tracing::trace!("end loop");
			return (prev, i);
		}
	}
}

fn fp_init_set(kind: mc::FpKind, lts: &Lts) -> StateSet {
	use mc::FpKind::*;
	match kind {
		Mu => StateSet::default(),
		Nu => lts.states().clone(),
	}
}

fn modal_init_set(op: mc::ModalKind, lts: &Lts) -> StateSet {
	use mc::ModalKind::*;
	match op {
		Diamond => StateSet::default(),
		Box => lts.states().clone(),
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use crate::test;
	use std::str::FromStr;
	use tracing::info;

	/// The Em-Lei implementation would accidently mishandle the `prev_fp` value,
	/// which caused the algorithm to forget to reset fixpoint values in certain cases.
	#[test]
	fn emlei_reset() {
		test::init();
		let lts = Lts::from_aldebaran(
			"des (2,16,4)\n\
			 (0,\"shared\",0)\n\
			 (0,\"won\",1)\n\
			 (0,\"others\",2)\n\
			 (0,\"others\",3)\n\
			 (1,\"req_exclusive\",0)\n\
			 (1,\"choose2\",1)\n\
			 (1,\"won\",2)\n\
			 (1,\"exclusive\",3)\n\
			 (2,\"choose1\",0)\n\
			 (2,\"wisdom\",1)\n\
			 (2,\"req_exclusive\",2)\n\
			 (2,\"plato\",3)\n\
			 (3,\"exclusive\",0)\n\
			 (3,\"plato\",1)\n\
			 (3,\"plato\",2)\n\
			 (3,\"shared\",3)",
		)
		.unwrap();
		let prop = include_str!("../../data/ccp/infinitely_often_exclusive.mcf");
		let f = Formula::from_str(prop).unwrap();
		let (mc_result, iter) = eval(&lts, &f);
		info!("em_lei iter: {iter}");
		assert_eq!(mc_result, StateSet::default());
	}
}
