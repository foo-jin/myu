use anyhow::Context;
use ariadne::{Color, Fmt, Label, ReportKind, Source};
use chumsky::error::SimpleReason;
use clap::{Parser, ValueEnum};
use myu::{
	calculus::{
		self as mc, basic, emerson_lei, parity,
		parser::{Cst, Spanned},
	},
	lts,
};
use rayon::prelude::*;
use rustc_hash::FxHashMap as HashMap;
use std::{
	fmt::Debug,
	fs::File,
	io::{self, Read, Write},
	ops::Range,
	path::{Path, PathBuf},
};
use tabled::Tabled;

type Report<'a> = ariadne::Report<(&'a str, Range<usize>)>;

/// A model-checker for Labeled Transition Systems using a subset of the modal μ-calculus.
#[derive(Parser)]
struct Args {
	/// File specifying the LTS to be verified in aldebaran format
	lts: PathBuf,
	/// Files specifying the formulas to check in modal μ-calculus.
	#[clap(required = true)]
	formulae: Vec<PathBuf>,
	/// The algorithm used to check the formulas.
	#[clap(long, value_enum)]
	algo: Algo,
}

#[derive(Copy, Clone, PartialEq, Eq, ValueEnum)]
enum Algo {
	Basic,
	EmLei,
	Parity,
}

fn run() -> anyhow::Result<()> {
	let args = Args::parse();

	let lts_str = read_file(&args.lts)?;
	let f_str: Vec<(String, String)> = args
		.formulae
		.iter()
		.map(|file| read_file(file).map(|s| (file.display().to_string(), s)))
		.collect::<Result<_, _>>()?;
	let (lts, formulae) = {
		let lts = lts::from_aldebaran(&lts_str)?;
		let result = {
			let mut formulae = vec![];
			let mut err_reports = vec![];
			for (path, f_str) in &f_str {
				match mc::from_str(f_str) {
					Ok(f) => formulae.push((path, f_str, f)),
					Err(errs) => err_reports.extend(extract_reports(path, f_str, errs)),
				};
			}

			match &err_reports[..] {
				[] => Ok(formulae),
				[_, ..] => Err(err_reports),
			}
		};

		match result {
			Ok(formulae) => (lts, formulae),
			Err(err_reports) => {
				let k = err_reports.len();
				for (p, src, report) in err_reports {
					report.eprint((p, Source::from(src))).unwrap()
				}
				anyhow::bail!(
					"could not perform model checking due to {k} previous error{}",
					if k > 1 { "s" } else { "" }
				)
			},
		}
	};

	static_analysis(&formulae)?;

	let results: Vec<Row> = formulae
		.into_par_iter()
		.map(|(path, _src, mcf)| {
			let mcf = mcf.into();
			let (result, iter) = match args.algo {
				Algo::Basic => basic(&lts, &mcf),
				Algo::EmLei => emerson_lei(&lts, &mcf),
				Algo::Parity => (parity(&lts, &mcf), 0),
			};

			let verified = result.contains(lts.init());
			Row::new(path, &mcf, iter, verified)
		})
		.collect();

	print_table(&results)?;
	Ok(())
}

fn extract_reports<'p, 's>(
	path: &'p str,
	src: &'s str,
	errs: Vec<chumsky::error::Simple<String>>,
) -> impl Iterator<Item = (&'p str, &'s str, Report<'p>)> {
	errs.into_iter().map(move |e| {
		let mut report = Report::build(ReportKind::Error, path, e.span().start);
		report = match e.reason() {
			SimpleReason::Unexpected => report
				.with_message(format!(
					"{}, expected '{}'",
					if e.found().is_some() {
						"Unexpected token in input"
					} else {
						"Unexpected EOF"
					},
					if e.expected().len() == 0 {
						unreachable!("Turn back from whence you came.")
					} else {
						e.expected()
							.map(|expected| match expected {
								Some(exp) => exp.to_string(),
								None => "EOF".to_owned(),
							})
							.collect::<Vec<_>>()
							.join(", ")
					}
				))
				.with_label(
					Label::new((path, e.span()))
						.with_message(format!(
							"Unexpected token '{}'",
							e.found().unwrap_or(&"EOF".to_string()).fg(Color::Red)
						))
						.with_color(Color::Red),
				),
			SimpleReason::Unclosed { span, delimiter } => report
				.with_message(format!(
					"Unclosed delimiter '{}'",
					delimiter.fg(Color::Yellow)
				))
				.with_label(
					Label::new((path, span.clone()))
						.with_message(format!(
							"Unclosed delimiter '{}'",
							delimiter.fg(Color::Yellow)
						))
						.with_color(Color::Yellow),
				)
				.with_label(Label::new((path, e.span())).with_message(format!(
					"Must be closed before this '{}'",
					e.found().unwrap_or(&"EOF".to_owned()).fg(Color::Red)
				))),
			SimpleReason::Custom(msg) => report.with_message(msg).with_label(
				Label::new((path, e.span()))
					.with_message(format!("{}", msg.fg(Color::Red)))
					.with_color(Color::Red),
			),
		};

		(path, src, report.finish())
	})
}

fn static_analysis(formulae: &Vec<(&String, &String, Spanned<Cst>)>) -> anyhow::Result<()> {
	for (path, src, f) in formulae {
		let mut declared = HashMap::default();
		let mut shadowed = vec![];
		let mut unbound = vec![];
		for Spanned(g, sp) in f.subformulas() {
			match g {
				Cst::Var { name } if !declared.contains_key(name) => unbound.push((name, sp)),
				Cst::Fixpoint { var, .. } if declared.contains_key(var) => shadowed.push((var, sp)),
				Cst::Fixpoint { var, .. } => {
					declared.insert(var, sp);
				},
				_ => (),
			}
		}

		if !(shadowed.is_empty() && unbound.is_empty()) {
			let mut err_reports = vec![];
			for (var, sp) in shadowed {
				let declaration_site = declared[var];
				let report = Report::build(ReportKind::Error, path.as_ref(), sp.start)
					.with_message("Shadowed variable")
					.with_label(
						Label::new((path.as_ref(), sp.clone()))
							.with_message(format!("Variable '{}' is already bound", var)),
					)
					.with_label(
						Label::new((path.as_ref(), declaration_site.clone()))
							.with_message(format!("Variable '{}' first bound here", var)),
					)
					.finish();
				err_reports.push((*path, *src, report));
			}

			for (var, sp) in unbound {
				let report = Report::build(ReportKind::Error, path.as_ref(), sp.start)
					.with_message("Unbound variable")
					.with_label(
						Label::new((path.as_ref(), sp.clone()))
							.with_message(format!("Variable '{}' is unbound", var)),
					)
					.finish();
				err_reports.push((*path, *src, report));
			}

			let k = err_reports.len();
			for (p, src, report) in err_reports {
				report.eprint((p.as_ref(), Source::from(src))).unwrap()
			}
			anyhow::bail!(
				"could not perform model checking due to {k} previous error{}",
				if k > 1 { "s" } else { "" }
			)
		}
	}
	Ok(())
}

fn main() {
	use owo_colors::OwoColorize;
	if let Err(e) = run() {
		if atty::is(atty::Stream::Stderr) {
			eprint!("{}", "[myu error]: ".red().bold())
		} else {
			eprint!("[myu error]: ")
		}
		eprintln!("{e:?}");
		std::process::exit(1);
	}
}

fn read_file<P: AsRef<Path> + Debug>(p: P) -> anyhow::Result<String> {
	let mut file = File::open(&p).with_context(|| format!("failed to open {:#?}", &p))?;
	let mut s = String::new();
	file.read_to_string(&mut s)
		.with_context(|| format!("failed to read from {:#?}", &p))?;
	Ok(s)
}

#[derive(Tabled)]
#[allow(non_snake_case)]
struct Row {
	property: String,
	ND: u16,
	AD: u16,
	dAD: u16,
	iter: u32,
	#[tabled(display_with = "checkmarkerize")]
	μ: bool,
}

impl Row {
	fn new(path: &str, f: &mc::Formula, iter: u32, μ: bool) -> Self {
		Row {
			property: path.to_owned(),
			ND: f.nesting_depth(),
			AD: f.alternation_depth(),
			dAD: f.dependent_ad(),
			iter,
			μ,
		}
	}
}

fn checkmarkerize(r: &bool) -> String {
	use owo_colors::OwoColorize;
	let tty = atty::is(atty::Stream::Stdout);
	match (r, tty) {
		(true, true) => format!("{}", '✓'.green().bold()),
		(true, false) => '✓'.to_string(),
		(false, true) => format!("{}", 'x'.red().bold()),
		(false, false) => 'x'.to_string(),
	}
}

fn print_table(rows: &[Row]) -> io::Result<()> {
	use tabled::{object::Columns, Alignment, Modify, Style, Table};
	let mut table =
		Table::new(rows).with(Modify::new(Columns::new(1..5)).with(Alignment::center()));
	table = if atty::is(atty::Stream::Stdout) {
		table.with(Style::psql())
	} else {
		table.with(Style::blank())
	};

	writeln!(io::stdout(), "{table}")
}
