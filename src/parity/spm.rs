use super::*;
use std::{cmp::Ordering, collections::BTreeMap, iter};
use Measure::*;

#[derive(Clone, Debug, Eq)]
enum Measure {
	Top,
	M(Box<[u32]>),
}

#[derive(Debug)]
struct Env {
	limits: Box<[u32]>,
	ms: BTreeMap<NodeIndex, Measure>,
}

#[must_use]
pub fn spm(game: &Game) -> Partition {
	let mut env = Env {
		limits: {
			let mut limits = vec![];
			for v in game.graph.node_weights() {
				let p = v.priority as usize;
				limits.resize_with(p + 1, Default::default);
				if !v.even_priority() {
					limits[p] += 1;
				}
			}

			limits.into_boxed_slice()
		},
		ms: {
			let mut ms = BTreeMap::default();
			for (ix, v) in game.graph.node_indices().zip(game.graph.node_weights()) {
				let m = iter::repeat(0)
					.take(v.priority as usize + 1)
					.collect::<Vec<_>>();
				ms.insert(ix, M(m.into()));
			}
			ms
		},
	};

	event!(Level::TRACE, ?env);
	let ixs = strategy::outgoing_oddprio(game);

	while let Some((v, m)) = ixs
		.iter()
		.find_map(|&v| lift(game, &env, v).map(|m| (v, m)))
	{
		env.ms.insert(v, m);
		event!(Level::TRACE, ?env);
	}

	let mut result = Partition::default();
	for (ix, m) in env.ms {
		match m {
			Top => result[Odd].insert(convert(ix)),
			M(_) => result[Even].insert(convert(ix)),
		};
	}
	result
}

pub mod strategy {
	use super::*;

	pub fn indices(g: &Game) -> Vec<NodeIndex> {
		g.graph.node_indices().collect()
	}

	pub fn shuffle_indices(g: &Game) -> Vec<NodeIndex> {
		use rand::prelude::SliceRandom;

		let mut ixs = g.graph.node_indices().collect::<Vec<_>>();
		let mut rng = rand::thread_rng();
		ixs.shuffle(&mut rng);
		ixs
	}

	pub fn incoming_indices(g: &Game) -> Vec<NodeIndex> {
		use petgraph::Direction::Incoming;

		let mut ixs = g.graph.node_indices().collect::<Vec<_>>();
		ixs.sort_unstable_by_key(|&ix| g.graph.neighbors_directed(ix, Incoming).count());
		ixs.reverse();
		ixs
	}

	pub fn outgoing_indices(g: &Game) -> Vec<NodeIndex> {
		use petgraph::Direction::Outgoing;

		let mut ixs = g.graph.node_indices().collect::<Vec<_>>();
		ixs.sort_unstable_by_key(|&ix| g.graph.neighbors_directed(ix, Outgoing).count());
		ixs
	}

	pub fn outgoing_cycle(g: &Game) -> Vec<NodeIndex> {
		let mut ixs = outgoing_indices(g);
		ixs.sort_by_key(|&ix| if g.graph.contains_edge(ix, ix) { 0 } else { 1 });
		ixs
	}

	pub fn outgoing_oddprio(g: &Game) -> Vec<NodeIndex> {
		let mut ixs = outgoing_indices(g);
		ixs.sort_by_key(|&ix| u32::from(g.graph[ix].even_priority()));
		ixs
	}

	pub fn cycled(g: &Game) -> Vec<NodeIndex> {
		use petgraph::algo;

		let mut ixs = outgoing_oddprio(g);
		ixs.sort_by_key(|&ix| {
			let kshort = algo::k_shortest_path(&g.graph, ix, Some(ix), 2, |_| 1);
			match kshort.get(&ix) {
				Some(&dist) => dist,
				None => g.graph.node_count() as u32,
			}
		});
		ixs
	}

	pub fn short_cycled(g: &Game) -> Vec<NodeIndex> {
		let mut ixs = outgoing_oddprio(g);
		ixs.sort_by_key(|&ix| {
			g.graph
				.neighbors(ix)
				.filter(|&jx| g.graph.contains_edge(ix, ix) || g.graph.contains_edge(jx, ix))
				.count()
		});
		ixs
	}
}

/// Attempts to lift `v`. Returns `Some` with the lifted measure if `v` is successfully lifted, `None`
/// otherwise.
#[tracing::instrument(skip(g))]
fn lift(g: &Game, env: &Env, v: NodeIndex) -> Option<Measure> {
	let it = g.graph.neighbors(v).map(|w| prog(g, env, v, w));
	let new = match g.graph[v].owner {
		Even => it.min(),
		Odd => it.max(),
	};

	let m = new?;
	event!(Level::TRACE, ?m);
	if m > env.ms[&v] {
		Some(m)
	} else {
		None
	}
}

/// Returns the least measure _m_ such that:
/// - p(v) is even :: m ≥ₚ₍ᵥ₎ ms[w]
/// - p(v) is odd :: m >ₚ₍ᵥ₎ ms[w], or, if ms[w] = Top then m = Top
#[tracing::instrument(skip(g, env))]
fn prog(g: &Game, env: &Env, v: NodeIndex, w: NodeIndex) -> Measure {
	let result = match env.ms[&w].clone() {
		Top => Top,
		M(m) => {
			let mut m = Vec::from(m);
			let p = g.graph[v].priority as usize;
			m.resize_with(p + 1, Default::default);

			if g.graph[v].even_priority() {
				M(m.into())
			} else if let Some((x, _)) = m
				.iter_mut()
				.zip(env.limits.iter().copied())
				.find(|(x, limit)| **x <= *limit)
			{
				*x += 1;
				M(m.into())
			} else {
				Top
			}
		},
	};
	event!(Level::TRACE, ?result);
	result
}

impl Ord for Measure {
	/// This method returns an [`Ordering`] between `self` and `other`.
	///
	/// `self.cmp(&other)` returns the lexicographic ordering on 'tuples' of equal length, limited/extended
	/// to the length of `self`. [`Measure::Top`] is greater than any element other than itself, which is
	/// considered equal.
	fn cmp(&self, other: &Self) -> Ordering {
		match (self, other) {
			(Top, Top) => Ordering::Equal,
			(Top, _) => Ordering::Greater,
			(_, Top) => Ordering::Less,
			(M(m1), M(m2)) => m1
				.iter()
				.cmp(m2.iter().chain(iter::repeat(&0)).take(m1.len())),
		}
	}
}

impl PartialOrd for Measure {
	/// Check [`Measure::cmp`] for the details of the ordering applicable to `Measure`.
	fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
		Some(self.cmp(other))
	}
}

impl PartialEq for Measure {
	/// Check [`Measure::cmp`] for the details of the ordering applicable to `Measure`.
	fn eq(&self, other: &Self) -> bool {
		matches!(self.cmp(other), Ordering::Equal)
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use crate::{rb, test::*};
	use enum_map::enum_map;

	fn check(game: Game, expected: Partition) {
		init();
		assert_eq!(spm(&game), expected);
	}

	#[test]
	fn ordering() {
		use Measure::*;

		assert!(M(vec![].into()) < Top);
		assert!(Top == Top);
		assert!(Top > M(vec![].into()));

		assert!(M(vec![1].into()) < M(vec![2].into()));
		assert!(M(vec![2].into()) == M(vec![2].into()));
		assert!(M(vec![3].into()) > M(vec![2].into()));

		assert!(M(vec![1].into()) == M(vec![1, 2].into()));
		assert!(M(vec![1, 0].into()) == M(vec![1].into()));
		assert!(M(vec![1, 1].into()) > M(vec![1].into()));
	}

	#[test]
	fn prog() {
		{
			// `prog` checked for < instead of <= causing it to skip valid lifts
			let g = construct([(Even, 1), (Even, 1), (Even, 2)], [(0, 2), (1, 1), (2, 2)]);
			let expected = Partition(enum_map! {
				Even => rb![0, 2],
				Odd => rb![1],
			});
			check(g, expected)
		}
		{
			// `prog` resized the measure to v.priority instead of v.priority + 1
			let g = construct(
				[
					(Even, 0),
					(Even, 0),
					(Even, 0),
					(Even, 1),
					(Even, 1),
					(Even, 1),
					(Even, 2),
				],
				[(0, 3), (1, 4), (2, 5), (3, 6), (4, 6), (5, 3), (6, 6)],
			);
			let expected = Partition(enum_map! {
				Even => (0..=6).collect(),
				Odd => rb![],
			});
			check(g, expected)
		}
	}
}
