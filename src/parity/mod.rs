mod spm;
mod zielonka;

pub use spm::{spm, strategy};
pub use zielonka::zielonka;

use enum_map::Enum;
use petgraph::{graph::NodeIndex, stable_graph::StableGraph};
use std::ops::{Index, IndexMut, Not};
use tracing::{event, Level};
use Player::*;

#[derive(Clone, Copy, Debug, Enum, Eq, PartialEq)]
pub enum Player {
	Even, // <>
	Odd,  // []
}

type Prio = u16;

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub struct Vertex {
	pub owner: Player,
	pub priority: Prio,
}

type NodeIx = u32;
pub(crate) type Graph = StableGraph<Vertex, ()>;
pub(crate) type NodeSet = roaring::RoaringBitmap;

#[derive(Debug)]
pub struct Game {
	graph: Graph,
}

impl Game {
	#[must_use]
	pub fn new(graph: Graph) -> Self {
		Game { graph }
	}

	#[must_use]
	pub fn graph(&self) -> &Graph {
		&self.graph
	}

	pub fn simplify(&mut self) {
		// self-loop elimination
		let mut eliminations = vec![];
		for (ix, n) in self
			.graph
			.node_indices()
			.zip(self.graph.node_weights().copied())
			// filter on self-loop
			.filter(|&(ix, _)| self.graph.neighbors(ix).any(|jx| ix == jx))
		{
			match (n.owner, n.even_priority()) {
				// eliminate self-loop
				(Even, false) | (Odd, true) => {
					eliminations.push(self.graph.find_edge(ix, ix).unwrap());
				},
				// retain only the self-loop
				_ => {
					eliminations.extend(
						self.graph
							.neighbors(ix)
							.filter(|jx| *jx != ix)
							.filter_map(|jx| self.graph.find_edge(ix, jx)),
					);
				},
			}
		}

		for e in eliminations {
			self.graph.remove_edge(e).unwrap();
		}
	}
}

impl Vertex {
	fn even_priority(&self) -> bool {
		self.priority % 2 == 0
	}
}

/// # Panics
///
/// Can panic when u32::try_from(usize) fails
fn convert(ix: NodeIndex<NodeIx>) -> NodeIx {
	u32::try_from(ix.index()).unwrap()
}

#[derive(Debug, Default, PartialEq)]
pub struct Partition(enum_map::EnumMap<Player, NodeSet>);

impl Index<Player> for Partition {
	type Output = NodeSet;

	fn index(&self, ix: Player) -> &Self::Output {
		&self.0[ix]
	}
}

impl IndexMut<Player> for Partition {
	fn index_mut(&mut self, ix: Player) -> &mut NodeSet {
		&mut self.0[ix]
	}
}

impl Not for Player {
	type Output = Self;

	fn not(self) -> Self::Output {
		match self {
			Even => Odd,
			Odd => Even,
		}
	}
}

impl From<(Player, u16)> for Vertex {
	fn from((owner, priority): (Player, u16)) -> Self {
		Vertex { owner, priority }
	}
}

#[cfg(test)]
fn construct<C: Into<Vertex>, V: IntoIterator<Item = C>, E: IntoIterator<Item = (usize, usize)>>(
	vs: V,
	edges: E,
) -> Game {
	let mut g = Graph::new();
	let vs: Vec<NodeIndex> = vs.into_iter().map(|d| g.add_node(d.into())).collect();
	for (a, b) in edges.into_iter().map(|(a, b)| (vs[a], vs[b])) {
		g.add_edge(a, b, ());
	}
	Game::new(g)
}
