use super::*;

#[tracing::instrument]
pub fn zielonka(game: &Game) -> Partition {
	inner(&game.graph)
}

fn inner(g: &Graph) -> Partition {
	if g.node_count() == 0 {
		// (∅, ∅)
		return Default::default();
	}

	// try to establish a dominion
	let min_prio = g.node_weights().map(|v| v.priority).min().unwrap();
	let player = match min_prio % 2 {
		0 => Even,
		_ => Odd,
	};
	let opponent = !player;
	let u = g
		.node_indices()
		.filter(|ix| g[*ix].priority == min_prio)
		.map(convert)
		.collect();

	let attr = attractor_set(g, player, u);
	assert!(!attr.is_empty());
	let mut split = inner(&complement(g, &attr));

	event!(Level::DEBUG, ?attr, ?split);

	if split[opponent].is_empty() {
		// player wins G \ A, then player wins all of G
		split[player] |= attr;
	} else {
		// partition[opponent] is a dominion in G \ A.
		// since player cannot leave G \ A, partition[opponent] is also a dominion in G.
		let attr = attractor_set(g, opponent, split[opponent].clone());
		split = inner(&complement(g, &attr));

		split[opponent] |= attr;
	}

	split
}

fn complement(g: &Graph, set: &NodeSet) -> Graph {
	g.filter_map(
		|ix, &n| set.contains(convert(ix)).not().then_some(n),
		|_, &e| Some(e),
	)
}

fn attractor_set(g: &Graph, p: Player, u: NodeSet) -> NodeSet {
	let mut attr = u;
	loop {
		let mut new = false;
		for ix in g.node_indices() {
			let mut iter = g.neighbors(ix).map(convert);

			let cond = if g[ix].owner == p {
				iter.any(|vx| attr.contains(vx))
			} else {
				iter.all(|vx| attr.contains(vx))
			};

			if cond {
				new |= attr.insert(convert(ix));
			}
		}

		if !new {
			break;
		}
	}

	attr
}

#[cfg(test)]
mod tests {
	use super::*;
	use crate::rb;
	use enum_map::enum_map;

	fn check(game: Game, expected: Partition) {
		let result = zielonka::zielonka(&game);
		assert_eq!(result, expected);
	}

	#[test]
	fn simple() {
		crate::test::init();
		{
			// slide 22
			let game = construct(
				[(Odd, 1), (Even, 2), (Even, 3)],
				[(0, 2), (0, 1), (1, 1), (1, 0), (2, 0)],
			);
			let expected = Partition(enum_map! {
				Even => rb![1],
				Odd => rb![0, 2],
			});
			check(game, expected);
		}
		{
			// slide 17
			let game = construct(
				[(Even, 3), (Even, 3), (Even, 3)],
				[(0, 0), (0, 1), (1, 2), (2, 2)],
			);
			let expected = Partition(enum_map! {
				Even => rb![],
				Odd => rb![0, 1, 2]
			});
			check(game, expected)
		}
		{
			// slide 18
			let game = construct(
				[(Even, 3), (Even, 3), (Even, 3), (Odd, 2), (Even, 2)],
				[(0, 0), (0, 1), (1, 2), (2, 2), (3, 0), (3, 4), (4, 3)],
			);
			let expected = Partition(enum_map! {
				Even => rb![],
				Odd => rb![0, 1, 2, 3, 4]
			});
			check(game, expected)
		}
		{
			#[rustfmt::skip]
			// slide 19
			let game = construct(
				[
					(Even, 3),
					(Even, 3),
					(Even, 3),
					(Odd, 2),
					(Even, 2),
					(Odd, 1),
					(Even, 1),
				],
				[
					(0, 0), (0, 1),
					(1, 2),
					(2, 2),
					(3, 0), (3, 4),
					(4, 3), (4, 5),
					(5, 5), (5, 6),
					(6, 1), (6, 3),
				],
			);
			let expected = Partition(enum_map! {
				Even => rb![],
				Odd => (0..=6).collect(),
			});
			check(game, expected)
		}
	}

	#[test]
	fn regression() {
		crate::test::init();
		let game = construct([(Even, 0), (Even, 0), (Even, 1)], [(0, 2), (1, 1), (2, 2)]);
		let expected = Partition(enum_map! {
			Even => rb![1],
			Odd => rb![0, 2],
		});
		check(game, expected)
	}
}
