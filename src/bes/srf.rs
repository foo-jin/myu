use super::{Bes, Equation, Expr, Fixpoint::*, Level, Logic, Variable};
use std::{collections::VecDeque, fmt, mem};
use tracing::{event, trace};

impl<V: Variable> Srf<V> {
	pub fn equations(&self) -> impl Iterator<Item = (u16, &Equation<V>)> {
		self.0.equations()
	}
}

pub type Trail = u8;
pub type Var<V> = (V, Trail);
pub type SrfEq<V> = Equation<Var<V>>;

const FALSUM: Trail = Trail::MAX;
const VERUM: Trail = Trail::MAX - 1;

/// A BES in Standard Recursive Form (SRF). SRF requires all rhs equations to adhere to the
/// following syntax:
///
/// `f := X | ⋁ F | ⋀ F`
///
/// where
/// - X is a proposition variable
/// - F is a non-empty set of proposition variables.
///
/// An SRF representation of a BES can be obtained by calling [`Bes::to_srf()`].
#[derive(Clone, Eq, PartialEq)]
pub struct Srf<V: Variable>(Bes<V>);

impl<V: Variable> From<Bes<V>> for Srf<Var<V>> {
	/// Repeatedly perform Tseitin transformation and constant elimination to transform the BES to
	/// Standard Recursive Form (SRF).
	fn from(bes: Bes<V>) -> Srf<Var<V>> {
		let (falsum, verum) = match bes.equations().next() {
			None => return Srf(Bes { equations: vec![] }),
			Some((_, eq)) => ((eq.var, FALSUM), (eq.var, VERUM)),
		};
		let mut srf = Srf(bes
			.equations
			.into_iter()
			.flat_map(|(_, eq)| eq.tseitin(falsum, verum).into_iter())
			.collect());

		if srf
			.0
			.equations()
			.flat_map(|(_, eq)| eq.variables())
			.any(|v| v == falsum)
		{
			srf = Srf(srf
				.0
				.equations
				.into_iter()
				.map(|(_, eq)| eq)
				.chain([Equation::new(Mu, falsum, Expr::Var(falsum))])
				.collect());
		}

		if srf
			.0
			.equations()
			.flat_map(|(_, eq)| eq.variables())
			.any(|v| v == verum)
		{
			srf = Srf(srf
				.0
				.equations
				.into_iter()
				.map(|(_, eq)| eq)
				.chain([Equation::new(Nu, verum, Expr::Var(verum))])
				.collect());
		}

		srf
	}
}

impl<V: Variable> Equation<V> {
	// TODO: consider avoiding allocation
	#[tracing::instrument]
	fn tseitin(self, falsum: Var<V>, verum: Var<V>) -> Vec<SrfEq<V>> {
		use Expr::*;

		let Equation { fp, var, expr, .. } = self;
		let expr = expr.trail_var(0);
		let mut trail = 1;

		let mut queue = VecDeque::from([Equation::new(fp, (var, 0), expr)]);
		let mut eqs = vec![];

		while let Some(mut current) = queue.pop_front() {
			let mut swapped = vec![];

			current.expr.tseitin((var, trail), &mut swapped);
			let len = swapped.len();
			assert!(trail + Trail::try_from(len).unwrap() < Trail::MAX);

			queue.reserve(len);
			for expr in swapped.into_iter() {
				trail += 1;
				let eq = Equation::new(fp, (var, trail - 1), expr);
				queue.push_back(eq);
			}

			eqs.push(current);
		}

		assert!((eqs.len() as Trail) < VERUM);

		for eq in &mut eqs {
			// const elimination
			match (fp, &eq.expr) {
				(Mu, False) | (Nu, True) => eq.expr = Var(eq.var),
				(_, False) => eq.expr = Var(falsum),
				(_, True) => eq.expr = Var(verum),
				_ => (),
			}
			eq.closed = false;
		}

		assert!(
			!eqs.is_empty(),
			"tseitin transformation should always result in at least one equation"
		);
		trace!("{eqs:?}");
		eqs
	}
}

impl<V: Variable> Expr<Var<V>> {
	/// Try to apply the tseitin transformation to the expression. Returns `Some` with the
	/// swapped-out expression if succesful, and `None` otherwise.
	#[tracing::instrument]
	fn tseitin(&mut self, mut var: Var<V>, swapped: &mut Vec<Self>) {
		use Expr::*;
		match self {
			And(_, _) => self.replace(Logic::And, &mut var, swapped),
			Or(_, _) => self.replace(Logic::Or, &mut var, swapped),
			_ => (),
		};

		event!(Level::TRACE, ?self, ?swapped);
	}

	#[tracing::instrument]
	fn replace(&mut self, outer: Logic, var: &mut Var<V>, swapped: &mut Vec<Self>) {
		use Expr::*;
		use Logic as L;

		let mut tmp = mem::replace(self, Var(*var));
		match (outer, &mut tmp) {
			(_, True | False | Var(_)) => (),
			(L::And, Or(_, _)) | (L::Or, And(_, _)) => {
				let tmp = mem::replace(&mut tmp, False);
				swapped.push(tmp);
				let (v, t) = *var;
				*var = (v, t + 1);
				event!(Level::TRACE, ?swapped);
				return;
			},
			(_, And(l, r) | Or(l, r)) => {
				l.replace(outer, var, swapped);
				r.replace(outer, var, swapped);
			},
		};
		mem::swap(&mut tmp, self);
		event!(Level::TRACE, ?self);
	}
}

impl<V: Variable> Expr<V> {
	fn trail_var(self, d: Trail) -> Expr<Var<V>> {
		use Expr::*;
		match self {
			True => True,
			False => False,
			Var(v) => Var((v, d)),
			And(f1, f2) => Expr::and(f1.trail_var(d), f2.trail_var(d)),
			Or(f1, f2) => Expr::or(f1.trail_var(d), f2.trail_var(d)),
		}
	}
}

impl<V: Variable> fmt::Display for Srf<V> {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "{}", self.0)
	}
}

impl<V: Variable> fmt::Debug for Srf<V> {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "{:?}", self.0)
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use crate::test::*;

	#[test]
	fn tseitin() {
		init();
		use Expr::*;
		let bes: Bes<char> = [
			(Mu, 'X', Expr::and('X', Expr::or('Y', 'Z'))),
			(Nu, 'Y', Expr::or('W', Expr::and('X', 'Y'))),
			(Mu, 'Z', False),
			(Mu, 'W', Expr::or('Z', Expr::or('Z', 'W'))),
		]
		.into_iter()
		.collect();
		let srf = Srf::from(bes);
		let expected = Srf([
			(Mu, ('X', 0), Expr::and(('X', 0), ('X', 1))),
			(Mu, ('X', 1), Expr::or(('Y', 0), ('Z', 0))),
			(Nu, ('Y', 0), Expr::or(('W', 0), ('Y', 1))),
			(Nu, ('Y', 1), Expr::and(('X', 0), ('Y', 0))),
			(Mu, ('Z', 0), Expr::from(('Z', 0))),
			(Mu, ('W', 0), Expr::or(('Z', 0), ('W', 0))),
		]
		.into_iter()
		.collect());
		assert_eq!(srf, expected);
	}
}
