mod gauss;
pub mod srf;

use crate::{calculus, Formula, Lts, State, StateSet};
pub use crate::{Fixpoint, Logic};
pub use srf::Srf;
use srf::Trail;
use std::{collections::VecDeque, fmt, hash::Hash, mem};
use tracing::{debug, error, trace, Level};
use Fixpoint::*;

pub trait Variable = Copy + Clone + fmt::Debug + Eq + PartialEq + Hash;

pub type VarState = (calculus::VarName, State);
pub type Rank = u16;

// TODO: Split terminals from non-terminals and force And|Or to only recurse on the right
#[derive(Clone, Eq, PartialEq)]
pub enum Expr<V: Variable = VarState> {
	True,
	False,
	Var(V),
	And(Box<Expr<V>>, Box<Expr<V>>),
	Or(Box<Expr<V>>, Box<Expr<V>>),
}

#[derive(Clone, Eq, PartialEq)]
pub struct Equation<V: Variable = VarState> {
	fp: Fixpoint,
	var: V,
	expr: Expr<V>,
	closed: bool,
}

#[derive(Clone, Eq, PartialEq)]
pub struct Bes<V: Variable = VarState> {
	equations: Vec<(Rank, Equation<V>)>,
}

#[derive(thiserror::Error, Debug)]
#[error("BES can only be constructed from formulae starting with a fixpoint operator")]
pub struct NoFixpoint;

impl TryFrom<(&Lts, &Formula)> for Bes<VarState> {
	type Error = NoFixpoint;

	fn try_from((lts, f): (&Lts, &Formula)) -> Result<Self, Self::Error> {
		if !matches!(f, Formula::Fixpoint { .. }) {
			return Err(NoFixpoint);
		}

		fn rhs(lts: &Lts, t: State, f: &Formula) -> Expr<VarState> {
			use Expr::*;
			match f {
				Formula::True => True,
				Formula::False => False,
				Formula::Var { name } => Var((*name, t)),
				Formula::Binary { op, f1, f2 } => {
					use calculus::BinOp::*;
					match op {
						And => Expr::and(rhs(lts, t, f1), rhs(lts, t, f2)),
						Or => Expr::or(rhs(lts, t, f1), rhs(lts, t, f2)),
					}
				},
				Formula::Modal { op, step, f: g } => {
					use calculus::ModalKind::*;
					let it = lts
						.step_transitions(*step)
						.filter_map(|(v, u)| (v == t).then_some(u))
						.map(move |u| rhs(lts, u, g));
					match op {
						Diamond => it.fold(False, Expr::or),
						Box => it.fold(True, Expr::and),
					}
				},
				// `rhs` gets called for each fixpoint, so those don't need to be constructed
				// recursively. Instead, we refer to the formula of that fixpoint by variable
				Formula::Fixpoint { var, .. } => Var((*var, t)),
			}
		}

		let bes = f
			.subformulas()
			.filter_map(|g| match g {
				Formula::Fixpoint { kind, var, f } => Some(lts.states().iter().map(|t| {
					let mut expr = rhs(lts, t, f);
					expr.simplify();
					Equation::new(*kind, (*var, t), expr)
				})),
				_ => None,
			})
			.flatten()
			.collect();

		Ok(bes)
	}
}

impl<V: Variable> Bes<V> {
	pub fn with_equations<E: Into<Equation<V>>, I: IntoIterator<Item = E>>(eqs: I) -> Bes<V> {
		eqs.into_iter().map(Into::into).collect()
	}

	pub fn equations(&self) -> impl Iterator<Item = (Rank, &Equation<V>)> {
		self.equations.iter().map(|(r, eq)| (*r, eq))
	}

	/// Repeatedly perform Tseitin transformation and constant elimination to transform the BES to
	/// Standard Recursive Form (SRF).
	#[must_use]
	pub fn to_srf(self) -> Srf<(V, Trail)> {
		self.into()
	}
}

impl Bes<VarState> {
	#[must_use]
	pub fn eval(self) -> StateSet {
		let gauss_results = self.gauss_elimination();
		gauss_results
			.first()
			.map(|x| x.0 .0)
			.map_or(StateSet::default(), |var| {
				gauss_results
					.into_iter()
					.take_while(|((v, _), _)| *v == var)
					.filter_map(|((_, s), b)| b.then_some(s))
					.collect()
			})
	}
}

impl<V: Variable> Equation<V> {
	pub fn new(fp: Fixpoint, var: V, expr: Expr<V>) -> Self {
		let closed = expr.is_closed();
		Equation {
			fp,
			var,
			expr,
			closed,
		}
	}

	pub fn is_disjunction(&self) -> bool {
		matches!(self.expr, Expr::Or(_, _))
	}

	pub fn is_conjunction(&self) -> bool {
		matches!(self.expr, Expr::And(_, _))
	}

	pub fn var(&self) -> V {
		self.var
	}

	pub fn variables(&self) -> impl Iterator<Item = V> + '_ {
		self.expr.variables()
	}

	#[tracing::instrument]
	fn simplify(&mut self) {
		if !self.closed {
			self.expr.simplify();
			self.closed = self.expr.is_closed();
			trace!("Result: {self:?}");
		}
	}
}

impl<V: Variable> Expr<V> {
	pub fn and<E1: Into<Self>, E2: Into<Self>>(f1: E1, f2: E2) -> Self {
		Expr::And(Box::new(f1.into()), Box::new(f2.into()))
	}

	pub fn or<E1: Into<Self>, E2: Into<Self>>(f1: E1, f2: E2) -> Self {
		Expr::Or(Box::new(f1.into()), Box::new(f2.into()))
	}

	pub fn iter(&self) -> impl Iterator<Item = &Self> {
		use Expr::*;
		let mut frontier = VecDeque::from([self]);
		std::iter::from_fn(move || {
			frontier.pop_front().map(|e| {
				match &e {
					True | False | Var(_) => (),
					And(l, r) | Or(l, r) => frontier.extend([&**l, &**r]),
				};
				e
			})
		})
	}

	pub fn variables(&self) -> impl Iterator<Item = V> + '_ {
		self.iter().filter_map(|e| match e {
			Expr::Var(v) => Some(*v),
			_ => None,
		})
	}

	fn is_closed(&self) -> bool {
		use Expr::*;
		match self {
			True | False => true,
			Var(_) => false,
			And(l, r) | Or(l, r) => l.is_closed() && r.is_closed(),
		}
	}

	#[tracing::instrument]
	fn simplify(&mut self) {
		use Expr::*;
		match self {
			And(f1, f2) | Or(f1, f2) => {
				f1.simplify();
				f2.simplify();
			},
			_ => (),
		};

		match self {
			And(l, r) => match (l.as_mut(), r.as_mut()) {
				// False ∧ X | X ∧ False
				(False, _) | (_, False) => {
					*self = False;
				},
				// True ∧ X | X ∧ True
				(True, other) | (other, True) => {
					let mut other = mem::replace(other, False);
					mem::swap(self, &mut other);
				},
				// X ∧ X
				(Var(v), Var(w)) if v == w => {
					*self = Var(*v);
				},
				// X ∧ ... ∧ X
				(Var(v), other @ And(_, _)) | (other @ And(_, _), Var(v)) => {
					other.dedup(Logic::And, *v);
				},
				_ => (),
			},
			Or(l, r) => match (l.as_mut(), r.as_mut()) {
				// True ∨ X | X ∨ True
				(True, _) | (_, True) => {
					*self = True;
				},
				// False ∨ X | X ∨ False
				(False, other) | (other, False) => {
					let mut other = mem::replace(other, False);
					mem::swap(self, &mut other);
				},
				// X ∨ X
				(Var(v), Var(w)) if v == w => {
					*self = Var(*v);
				},
				// X ∨ ... ∨ X
				(Var(v), other @ Or(_, _)) | (other @ Or(_, _), Var(v)) => {
					other.dedup(Logic::Or, *v);
				},
				_ => (),
			},
			_ => (),
		};

		trace!("{self:?}");
	}

	#[tracing::instrument]
	fn dedup(&mut self, outer: Logic, v: V) {
		use Expr::*;
		use Logic as L;

		let dummy = mem::replace(self, False);
		let result = match (outer, dummy) {
			(L::And, And(f, other) | And(other, f)) | (L::Or, Or(f, other) | Or(other, f))
				if *f == Var(v) =>
			{
				let mut other = *other;
				debug!("{other}");
				other.dedup(outer, v);
				other
			},
			(_, dummy) => dummy,
		};

		let _ = mem::replace(self, result);
		trace!("{self}");
	}
}

impl<V: Variable, E: Into<Equation<V>>> FromIterator<E> for Bes<V> {
	fn from_iter<T: IntoIterator<Item = E>>(iter: T) -> Self {
		let mut block = 1;
		let mut sign = None;
		// modifier ensures Mu blocks are always even and Nu blocks are always odd
		let mut modifier = 0;
		let equations = iter
			.into_iter()
			.map(Into::into)
			.map(|mut eq| {
				eq.simplify();
				eq
			})
			.map(|eq| {
				match sign {
					// `eq` is the outermost fixpoint
					None => match eq.fp {
						Mu => modifier = 0,
						Nu => modifier = 1,
					},
					Some(prev) if prev != eq.fp => block += 1,
					_ => (),
				}
				sign = Some(eq.fp);
				let rank = block - modifier;
				(rank, eq)
			})
			.collect();
		let result = Bes { equations };
		debug!("\n{result:?}");
		result
	}
}

impl<V: Variable> From<V> for Expr<V> {
	fn from(v: V) -> Self {
		Expr::Var(v)
	}
}

impl<V: Variable> fmt::Display for Expr<V> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		use Expr::*;

		match self {
			True => write!(f, "⊤"),
			False => write!(f, "⊥"),
			Var(v) => write!(f, "{v:?}"),
			And(l, r) => write!(f, "({} ∧ {})", l, r),
			Or(l, r) => write!(f, "({} ∨ {})", l, r),
		}
	}
}

impl<V: Variable> fmt::Debug for Expr<V> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "{}", self)
	}
}

impl<V: Variable> fmt::Display for Equation<V> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		let fp = match self.fp {
			Mu => "μ",
			Nu => "ν",
		};
		write!(f, "{fp} ")?;
		write!(f, "{:?} = ", self.var)?;
		write!(f, "{}", self.expr)
	}
}

impl<V: Variable> fmt::Debug for Equation<V> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "{self}{}", if self.closed { "." } else { "" })
	}
}

impl<V: Variable> fmt::Display for Bes<V> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		let joined = self
			.equations
			.iter()
			.map(|(rank, eq)| format!("{rank} | {eq}"))
			.collect::<Vec<_>>()
			.join("\n");
		write!(f, "{joined}")
	}
}

impl<V: Variable> fmt::Debug for Bes<V> {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		let joined = self
			.equations
			.iter()
			.map(|(rank, eq)| format!("{rank} | {eq:?}"))
			.collect::<Vec<_>>()
			.join("\n");
		write!(f, "{joined}")
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use crate::test::*;

	impl<V: Variable, E: Into<Expr<V>>> From<(Fixpoint, V, E)> for Equation<V> {
		fn from((fp, var, expr): (Fixpoint, V, E)) -> Self {
			Equation::new(fp, var, expr.into())
		}
	}

	fn simple_bes() -> Bes<VarState> {
		use Expr::*;
		[
			(
				Nu,
				('X', 0),
				And(
					Box::new(Var(('X', 1))),
					Box::new(Or(Box::new(Var(('Y', 0))), Box::new(Var(('Z', 0))))),
				),
			),
			(
				Nu,
				('X', 1),
				Or(Box::new(Var(('Y', 1))), Box::new(Var(('Z', 1)))),
			),
			(
				Nu,
				('X', 2),
				Or(Box::new(Var(('Y', 2))), Box::new(Var(('Z', 2)))),
			),
			(Nu, ('Y', 0), False),
			(Nu, ('Y', 1), Var(('Y', 2))),
			(Nu, ('Y', 2), Var(('Y', 1))),
			(Mu, ('Z', 0), True),
			(Mu, ('Z', 1), True),
			(Mu, ('Z', 2), Var(('Z', 1))),
		]
		.into_iter()
		.collect()
	}

	#[test]
	fn simple() {
		init();
		let bes = simple_bes();
		let (_, result) = bes.gauss_elimination()[0];
		assert!(result);
	}

	#[test]
	fn conversion() {
		use std::convert::TryFrom;
		init();
		let f: Formula = {
			let s = "nu X. [r]X && (nu Y. <tau>Y || <l>Y) || mu Z. [l]Z && [s]Z || <s>true";
			s.parse().unwrap()
		};
		let lts: Lts = {
			let s = "des (0,4,3)\n\
				   (0,\"r\",1)\n\
				   (1,\"s\",0)\n\
				   (1,\"tau\",2)\n\
				   (2,\"l\",1)";
			Lts::from_aldebaran(s).unwrap()
		};
		let result = Bes::try_from((&lts, &f)).unwrap();
		let expected = simple_bes();
		assert_eq!(result, expected);
	}

	#[test]
	fn rank() {
		init();
		let ranks = simple_bes()
			.equations()
			.map(|(rank, _)| rank)
			.collect::<Vec<_>>();
		assert_eq!(&ranks, &[0, 0, 0, 0, 0, 0, 1, 1, 1]);
	}
}
