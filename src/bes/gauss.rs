use super::{Bes, Equation, Expr, Variable};
use crate::Fixpoint;
use std::mem;
use tracing::{debug, event, trace, trace_span as span, Level};
use Fixpoint::*;

impl<V: Variable> Bes<V> {
	#[must_use]
	pub fn gauss_elimination(self) -> Vec<(V, bool)> {
		if self.equations.is_empty() {
			return vec![];
		}

		let mut results = {
			let mut eqs = self.equations;
			let mut results = vec![];
			while let Some((_, mut eq)) = eqs.pop() {
				let span = span!("gauss main loop", ?eq.var);
				let _e = span.enter();
				event!(Level::DEBUG, %eq);

				let sub = match eq.fp {
					Mu => Expr::False,
					Nu => Expr::True,
				};
				eq.substitute(eq.var, &sub);

				for (_, other) in &mut eqs {
					let span = span!("inner loop", ?other.var);
					let _e = span.enter();

					other.substitute(eq.var, &eq.expr);
				}

				debug!("{eq:?}");
				results.push(eq);
			}
			results
		};

		results.reverse();
		for i in 0..results.len() - 1 {
			let (l, rest) = results.split_at_mut(i + 1);
			let eq = &l[i];
			for other in rest {
				other.substitute(eq.var, &eq.expr);
			}
		}

		let results = results
			.into_iter()
			.inspect(|eq| assert!(eq.closed))
			.map(|eq| (eq.var, eq.expr.eval()))
			.collect();

		tracing::info!("{results:?}");
		results
	}
}

impl<V: Variable> Equation<V> {
	fn substitute(&mut self, var: V, sub: &Expr<V>) {
		if !self.closed {
			trace!("substituting '{:?}' <- '{:?}'", var, sub);
			for loc in self.expr.substitute(var) {
				let mut sub = sub.clone();
				mem::swap(loc, &mut sub);
			}
			self.simplify();
		}
	}
}

impl<V: Variable> Expr<V> {
	// TODO: take mut vec as input
	fn substitute(&mut self, target: V) -> Vec<&mut Self> {
		use Expr::*;

		match self {
			Var(name) if *name == target => vec![self],
			And(f1, f2) | Or(f1, f2) => {
				let mut result = f1.substitute(target);
				result.append(&mut f2.substitute(target));
				result
			},
			_ => vec![],
		}
	}

	fn eval(&self) -> bool {
		use Expr::*;
		match self {
			True => true,
			False => false,
			And(f1, f2) => f1.eval() && f2.eval(),
			Or(f1, f2) => f1.eval() || f2.eval(),
			v @ Var(_) => panic!("cannot evaluate open expression: '{v:?}' found"),
		}
	}
}
