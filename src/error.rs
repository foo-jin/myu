use thiserror::Error;

#[derive(Error, Debug)]
#[error("failed to parse μ-calculus formula")]
pub struct McfParseError(#[from] anyhow::Error);

#[derive(Error, Debug)]
#[error("failed to parse labeled transition system")]
pub struct LtsParseError(#[from] anyhow::Error);

#[derive(Error, Debug)]
#[error("{0}")]
pub enum ParseError {
	Lts(#[from] LtsParseError),
	Mcf(#[from] McfParseError),
}
