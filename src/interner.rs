//! This module is based on a blogpost by matklad:
//! <https://matklad.github.io/2020/03/22/fast-simple-rust-interner.html>

use crate::HashMap;
use once_cell::sync::Lazy;
use std::{convert::TryFrom, mem, sync::RwLock};

pub(crate) static INTRN: Lazy<RwLock<Interner>> =
	Lazy::new(|| RwLock::new(Interner::with_capacity(128)));

/// Represents the ID of a string in a string pool.
#[derive(Clone, Copy, Eq, PartialEq, Hash)]
pub struct StrId(u16);

/// Inserts a string in the global string pool.
///
/// # Panics
/// Can panic when the amount of `StrId`s issued by the interner reaches u16::MAX.
#[must_use]
pub fn intern(name: &str) -> StrId {
	INTRN.write().unwrap().intern(name)
}

/// Returns a reference to the string corresponding to the ID stored in the global string pool.
#[must_use]
pub fn lookup(id: StrId) -> &'static str {
	INTRN.read().unwrap().lookup(id)
}

pub(crate) struct Interner {
	map: HashMap<&'static str, StrId>,
	vec: Vec<&'static str>,
	buf: String,
	full: Vec<String>,
}

impl Interner {
	pub fn with_capacity(cap: usize) -> Interner {
		let cap = cap.next_power_of_two();
		Interner {
			map: HashMap::default(),
			vec: Vec::new(),
			buf: String::with_capacity(cap),
			full: Vec::new(),
		}
	}

	/// # Panics
	/// Can panic when the amount of `StrId`s issued by the interner reaches u16::MAX.
	pub fn intern(&mut self, name: &str) -> StrId {
		if let Some(&id) = self.map.get(name) {
			return id;
		}
		let name = unsafe { self.alloc(name) };
		let id = u16::try_from(self.map.len()).unwrap();
		let id = StrId(id);
		self.map.insert(name, id);
		self.vec.push(name);

		debug_assert!(self.lookup(id) == name);
		debug_assert!(self.intern(name) == id);

		id
	}

	pub fn lookup(&self, id: StrId) -> &'static str {
		self.vec[usize::from(id.0)]
	}

	unsafe fn alloc(&mut self, name: &str) -> &'static str {
		let cap = self.buf.capacity();
		if cap < self.buf.len() + name.len() {
			let new_cap = (cap.max(name.len()) + 1).next_power_of_two();
			let new_buf = String::with_capacity(new_cap);
			let old_buf = mem::replace(&mut self.buf, new_buf);
			self.full.push(old_buf);
		}

		let interned = {
			let start = self.buf.len();
			self.buf.push_str(name);
			&self.buf[start..]
		};

		&*(interned as *const str)
	}
}

impl std::fmt::Display for StrId {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		let s = INTRN.read().map_err(|_| std::fmt::Error)?.lookup(*self);
		f.write_str(s)
	}
}

impl std::fmt::Debug for StrId {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		let s = INTRN.read().map_err(|_| std::fmt::Error)?.lookup(*self);
		f.write_str(s)
	}
}
