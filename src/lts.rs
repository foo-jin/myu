pub use crate::error::LtsParseError as ParseError;
use crate::{
	interner::{self, StrId},
	HashMap, Label, State, StateSet,
};
use anyhow::anyhow;
use combine::{
	between, eof,
	parser::{
		char::{char, newline, space, spaces, string},
		range::take_while1,
	},
	skip_many, skip_many1,
	stream::position,
	EasyParser, Parser,
};

pub type Graph = HashMap<Label, Vec<(State, State)>>;

#[derive(Clone, Debug, PartialEq)]
pub struct Lts {
	init: State,
	states: StateSet,
	trans: Graph,
}

impl Lts {
	#[must_use]
	pub fn new(init: State, n: u32) -> Self {
		let mut states = StateSet::default();
		states.insert_range(..n);
		let trans = Graph::default();
		Lts {
			init,
			states,
			trans,
		}
	}

	#[must_use]
	pub fn states(&self) -> &StateSet {
		&self.states
	}

	#[must_use]
	pub fn transition_count(&self) -> usize {
		self.trans
			.iter()
			.flat_map(|(_, trans)| trans.iter())
			.count()
	}

	pub fn transitions(&self) -> impl Iterator<Item = (Label, &[(State, State)])> + '_ {
		self.trans.iter().map(|(l, ts)| (*l, ts.as_ref()))
	}

	pub fn step_transitions(&self, step: Label) -> impl Iterator<Item = (State, State)> + '_ {
		self.trans.get(&step).into_iter().flatten().copied()
	}

	#[must_use]
	pub fn init(&self) -> State {
		self.init
	}

	pub fn add_edge(&mut self, start: State, label: StrId, end: State) {
		assert!(
			self.states.contains(start),
			"cannot add edge from unknown state: {start}"
		);
		assert!(
			self.states.contains(end),
			"cannot add edge to unknown state: {end}"
		);

		self.trans.entry(label).or_default().push((start, end));
	}

	pub fn from_aldebaran(s: &str) -> Result<Self, ParseError> {
		from_aldebaran(s)
	}

	// pub fn prune(&mut self, f: &Formula) {
	// 	let labels: HashSet<Label> = self.transitions().map(|(l, _)| l).collect();
	// 	labels.difference(&f.actions().collect::<HashSet<Label>>()).copied().for_each(|a| {
	// 		self.purge_action(a);
	// 	});
	// }

	pub fn purge_action(&mut self, action: Label) -> bool {
		if let Some(ts) = self.trans.remove(&action) {
			if ts.is_empty() {
				return false;
			}

			let mut states = StateSet::default();
			for (s, t) in self
				.trans
				.iter()
				.flat_map(|(_, trans)| trans.iter())
				.copied()
			{
				states.extend([s, t]);
			}
			self.states = states;
			return true;
		}

		false
	}

	pub fn purge_state(&mut self, s: State) -> bool {
		if self.states.remove(s) {
			self.trans.iter_mut().for_each(|(_, ts)| {
				ts.retain(|(s, t)| self.states.contains(*s) && self.states.contains(*t))
			});

			true
		} else {
			false
		}
	}
}

pub fn from_aldebaran(s: &str) -> Result<Lts, ParseError> {
	let int = || combine::from_str(take_while1(|c: char| c.is_ascii_digit()));
	let non_newline_spaces = || skip_many(char(' ').or(char('\t')));
	let aut_header = || {
		(
			string("des").skip(skip_many1(space())).skip(char('(')),
			int().skip(char(',')),
			int().skip(char(',')),
			int().skip(char(')')),
		)
	};
	let aut_edge = || {
		between(
			char('('),
			char(')'),
			(
				int(),
				between(
					string(r#",""#),
					string(r#"","#),
					take_while1(|c: char| c != '"'),
				),
				int(),
			),
		)
	};

	let ((_, initial, _n_trans, n_states), mut s) = aut_header()
		.easy_parse(position::Stream::new(s))
		.map_err(|e| anyhow!("{e}"))?;

	let mut lts = Lts::new(initial, n_states);
	{
		let mut intrn = interner::INTRN.write().unwrap();
		while let Ok((_, mut rest)) = non_newline_spaces()
			.and(newline())
			.skip(spaces())
			.easy_parse(s)
		{
			if eof().parse(&mut rest).is_ok() {
				break;
			}

			let ((start, label, end), rest) =
				aut_edge().easy_parse(rest).map_err(|e| anyhow!("{e}"))?;
			let label_id = intrn.intern(label);
			lts.add_edge(start, label_id, end);
			s = rest;
		}
	}
	Ok(lts)
}

impl Eq for Lts {}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn parsing() {
		let input = r#"des (0,12,10)
(0,"lock(p2, f2)",1)
(0,"lock(p1, f1)",2)
(1,"lock(p1, f1)",3)
(1,"lock(p2, f1)",4)
(2,"lock(p2, f2)",3)
(2,"lock(p1, f2)",5)
(4,"eat(p2)",6)
(5,"eat(p1)",7)
(6,"free(p2, f2)",8)
(7,"free(p1, f1)",9)
(8,"free(p2, f1)",0)
(9,"free(p1, f2)",0)"#;

		{
			let result = from_aldebaran(input).unwrap();
			let mut intrn = interner::INTRN.write().unwrap();
			let mut expected = Lts::new(0, 10);
			expected.add_edge(0, intrn.intern("lock(p2, f2)"), 1);
			expected.add_edge(0, intrn.intern("lock(p1, f1)"), 2);
			expected.add_edge(1, intrn.intern("lock(p1, f1)"), 3);
			expected.add_edge(1, intrn.intern("lock(p2, f1)"), 4);
			expected.add_edge(2, intrn.intern("lock(p2, f2)"), 3);
			expected.add_edge(2, intrn.intern("lock(p1, f2)"), 5);
			expected.add_edge(4, intrn.intern("eat(p2)"), 6);
			expected.add_edge(5, intrn.intern("eat(p1)"), 7);
			expected.add_edge(6, intrn.intern("free(p2, f2)"), 8);
			expected.add_edge(7, intrn.intern("free(p1, f1)"), 9);
			expected.add_edge(8, intrn.intern("free(p2, f1)"), 0);
			expected.add_edge(9, intrn.intern("free(p1, f2)"), 0);

			assert_eq!(result, expected);
		}

		let input = "des (0,12,10)        \n\
(0,\"i\",1)
(0,\"i\",2)
(1,\"i\",3)
(1,\"i\",4)
(2,\"i\",5)
(2,\"i\",4)
(3,\"others\",6)
(5,\"plato\",7)
(6,\"i\",8)
(7,\"i\",9)
(8,\"i\",0)
(9,\"i\",0)
";
		{
			let result = from_aldebaran(input).unwrap();
			let mut intrn = interner::INTRN.write().unwrap();
			let mut expected = Lts::new(0, 10);
			expected.add_edge(0, intrn.intern("i"), 1);
			expected.add_edge(0, intrn.intern("i"), 2);
			expected.add_edge(1, intrn.intern("i"), 3);
			expected.add_edge(1, intrn.intern("i"), 4);
			expected.add_edge(2, intrn.intern("i"), 5);
			expected.add_edge(2, intrn.intern("i"), 4);
			expected.add_edge(3, intrn.intern("others"), 6);
			expected.add_edge(5, intrn.intern("plato"), 7);
			expected.add_edge(6, intrn.intern("i"), 8);
			expected.add_edge(7, intrn.intern("i"), 9);
			expected.add_edge(8, intrn.intern("i"), 0);
			expected.add_edge(9, intrn.intern("i"), 0);

			assert_eq!(result, expected);
		}
	}
}
