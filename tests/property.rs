#![feature(let_chains)]

use myu::{bes::Bes, interner::intern, lts, Formula, Label, State};
use quickcheck::{Arbitrary, Gen, TestResult};
use rand::{seq::SliceRandom, Rng};
use tracing::{error, event, Level};

const ACT: &[&str] = &[
	"won",
	"choose1",
	"choose2",
	"req_exclusive",
	"req_shared",
	"exclusive",
	"shared",
	"i",
	"ask",
	"wisdom",
	"playing",
	"plato",
	"others",
];

const DINING_PROPS: &[&str] = &[
	include_str!("../data/dining/invariantly_inevitably_eat.mcf"),
	include_str!("../data/dining/invariantly_plato_starves.mcf"),
	include_str!("../data/dining/invariantly_possibly_eat.mcf"),
	include_str!("../data/dining/plato_infinitely_often_can_eat.mcf"),
];

const DEMANDING_PROPS: &[&str] = &[
	// demanding children
	include_str!("../data/demanding/invariantly_possibly_ask.mcf"),
	include_str!("../data/demanding/no_answer_before_question.mcf"),
];

const CCP_PROPS: &[&str] = &[
	// cache coherence protocol
	include_str!("../data/ccp/infinite_run_no_access.mcf"),
	include_str!("../data/ccp/infinitely_often_exclusive.mcf"),
	include_str!("../data/ccp/invariantly_eventually_fair_shared_access.mcf"),
	include_str!("../data/ccp/invariantly_inevitably_exclusive_access.mcf"),
	include_str!("../data/ccp/invariantly_possibly_exclusive_access.mcf"),
];

const BOARD_PROPS: &[&str] = &[
	// boardgame
	include_str!("../data/boardgame/possibly_win.mcf"),
	include_str!("../data/boardgame/certainly_win.mcf"),
	include_str!("../data/boardgame/infinitely_win.mcf"),
];

type Graph = petgraph::graph::DiGraph<(), ()>;

#[derive(Clone, Debug, Eq, PartialEq)]
struct Lts(lts::Lts);

impl Arbitrary for Lts {
	fn arbitrary<G: Gen>(g: &mut G) -> Self {
		let graph = Graph::arbitrary(g);
		let n = graph.node_count() as State;
		let mut lts = lts::Lts::new(0, n);
		if n > 0 {
			let init = g.gen_range(0, n);
			lts = lts::Lts::new(init, n);
			for (s, t) in graph
				.raw_edges()
				.iter()
				.map(|e| (e.source().index() as State, e.target().index() as State))
			{
				let act = ACT.choose(g).unwrap();
				lts.add_edge(s, intern(act), t);
			}
		};

		Lts(lts)
	}

	#[allow(clippy::needless_collect)]
	fn shrink(&self) -> Box<dyn Iterator<Item = Self>> {
		error!("shrinking");
		let lts = self.0.clone();

		let states = self.0.states().clone();
		let labels: Vec<Label> = self.0.transitions().map(|(l, _)| l).collect();
		let lbl_purged = labels.into_iter().filter_map(move |l| {
			let mut lts = lts.clone();
			lts.purge_action(l).then_some(Lts(lts))
		});

		let lts = self.0.clone();
		let state_purge = states.into_iter().filter_map(move |s| {
			let mut lts = lts.clone();
			lts.purge_state(s).then_some(Lts(lts))
		});

		Box::new(lbl_purged.into_iter().chain(state_purge))
	}
}

#[derive(Clone, Debug)]
struct Dining(Formula);

impl Arbitrary for Dining {
	fn arbitrary<G: Gen>(g: &mut G) -> Self {
		let prop = DINING_PROPS.choose(g).unwrap();
		Dining(prop.parse().unwrap())
	}
}

#[derive(Clone, Debug)]
struct Demanding(Formula);

impl Arbitrary for Demanding {
	fn arbitrary<G: Gen>(g: &mut G) -> Self {
		let prop = DEMANDING_PROPS.choose(g).unwrap();
		Demanding(prop.parse().unwrap())
	}
}

#[derive(Clone, Debug)]
struct Ccp(Formula);

impl Arbitrary for Ccp {
	fn arbitrary<G: Gen>(g: &mut G) -> Self {
		let prop = CCP_PROPS.choose(g).unwrap();
		Ccp(prop.parse().unwrap())
	}
}

#[derive(Clone, Debug)]
struct Board(Formula);

impl Arbitrary for Board {
	fn arbitrary<G: Gen>(g: &mut G) -> Self {
		let prop = BOARD_PROPS.choose(g).unwrap();
		Board(prop.parse().unwrap())
	}
}

fn init() {
	use std::sync::Once;
	use tracing_subscriber::EnvFilter;
	static TEST_LOGS: Once = Once::new();
	TEST_LOGS.call_once(|| {
		let _ = tracing_subscriber::fmt()
			.pretty()
			.with_test_writer()
			.with_env_filter(EnvFilter::from_default_env())
			.try_init();
	});
}

mod equivalence {
	use super::*;
	use std::fs::File;

	macro_rules! gen_propchecks {
		($t:ty) => {
			use crate::{Board, Ccp, Demanding, Dining, Lts};
			use quickcheck_macros::quickcheck;

			#[quickcheck]
			fn dining(lts: Lts, f: Dining) -> $t {
				check(lts.0, f.0)
			}

			#[quickcheck]
			fn demanding(lts: Lts, f: Demanding) -> $t {
				check(lts.0, f.0)
			}

			#[quickcheck]
			fn ccp(lts: Lts, f: Ccp) -> $t {
				check(lts.0, f.0)
			}

			#[quickcheck]
			fn boardgame(lts: Lts, f: Board) -> $t {
				check(lts.0, f.0)
			}
		};
	}

	mod em_lei {
		use super::*;

		gen_propchecks!(bool);

		fn check(lts: lts::Lts, f: Formula) -> bool {
			init();
			// lts.prune(&f);
			let (basic_result, basic_iter) = myu::calculus::basic(&lts, &f);

			let (el_result, el_iter) = myu::calculus::emerson_lei(&lts, &f);

			let cond = basic_result == el_result && el_iter <= basic_iter;
			if !cond {
				event!(Level::INFO, %f);
				event!(Level::INFO, ?lts);
				event!(Level::INFO, ?basic_result, ?basic_iter);
				event!(Level::INFO, ?el_result, ?el_iter);
			}
			cond
		}
	}

	mod parity {
		use super::*;
		use std::io::Write;

		gen_propchecks!(bool);

		fn check(lts: lts::Lts, f: Formula) -> bool {
			init();
			let (lts_result, _) = myu::calculus::emerson_lei(&lts, &f);

			let bes = Bes::try_from((&lts, &f)).unwrap();
			let srf = bes.to_srf();

			let parity = myu::calculus::parity::Mapped::from(&srf);
			let parity_result = parity.eval();

			let cond = lts_result == parity_result;
			if !cond {
				use petgraph::dot::{Config, Dot};
				use xshell::{cmd, Shell};

				event!(Level::INFO, %f);
				event!(Level::INFO, ?lts);
				event!(Level::INFO, %srf);
				event!(Level::INFO, ?parity);
				event!(Level::INFO, ?lts_result);
				event!(Level::INFO, ?parity_result);

				let sh = Shell::new().expect("failed to spawn shell");
				let graph = parity.graph();
				let dot = Dot::with_config(&graph, &[Config::EdgeNoLabel]);
				let mut svg = File::create("parity.svg").expect("failed to create 'parity.svg'");
				let img = cmd!(sh, "fdp -Tsvg")
					.stdin(format!("{dot:?}"))
					.read()
					.unwrap();
				svg.write_all(img.as_ref())
					.expect("failed to write 'parity.svg'");
			}

			cond
		}
	}

	mod bes {
		use super::*;

		gen_propchecks!(TestResult);

		fn check(lts: lts::Lts, f: Formula) -> TestResult {
			init();
			if lts.transition_count() > 30 {
				return TestResult::discard();
			}

			event!(Level::INFO, %f);
			event!(Level::INFO, ?lts);

			let (lts_result, _) = myu::calculus::emerson_lei(&lts, &f);

			let bes = Bes::try_from((&lts, &f)).unwrap();
			let bes_result = bes.eval();

			event!(Level::INFO, ?lts_result);
			event!(Level::INFO, ?bes_result);

			TestResult::from_bool(lts_result == bes_result)
		}
	}
}
