use insta::assert_debug_snapshot as snapshot;
use myu::{lts, StateSet};
use once_cell::sync::Lazy;
use std::sync::Once;

macro_rules! set_snapshot_suffix {
    ($($expr:expr),*) => {
        let mut settings = insta::Settings::clone_current();
        settings.set_snapshot_suffix(format!($($expr,)*));
        let _guard = settings.bind_to_scope();
    }
}

fn init() {
	static TEST_LOGS: Once = Once::new();
	TEST_LOGS.call_once(|| {
		let subscriber = tracing_subscriber::fmt()
			.pretty()
			.with_test_writer()
			.with_max_level(tracing::Level::DEBUG)
			.finish();
		tracing::subscriber::set_global_default(subscriber)
			.expect("failed to set global subscriber");
	});
}

mod em_lei {
	use super::*;
	const LTS: &str = r#"des (0,14,8)
(0,"tau",1)
(0,"tau",2)
(1,"tau",3)
(1,"tau",4)
(2,"tau",5)
(2,"tau",4)
(3,"b",0)
(3,"a",6)
(4,"tau",7)
(4,"tau",6)
(5,"a",0)
(5,"a",7)
(6,"tau",2)
(7,"b",1)"#;

	static T: Lazy<StateSet> = Lazy::new(|| StateSet::from_sorted_iter(0..=7).unwrap());
	static F: Lazy<StateSet> = Lazy::new(StateSet::default);

	fn eval(formula: &str) -> (StateSet, u32) {
		init();
		let lts = lts::from_aldebaran(LTS).unwrap();
		let f = formula.parse::<myu::calculus::Formula>().unwrap();
		myu::calculus::emerson_lei(&lts, &f)
	}

	fn check(formula: &str, expected: &StateSet) {
		let (result, _) = eval(formula);
		tracing::debug!("{:?}", &result);
		assert_eq!(&result, expected, "{formula}")
	}

	#[test]
	fn boolean() {
		check("false", &F);
		check("true", &T);
		check("(false && true)", &F);
		check("(true && false)", &F);
		check("(true && true)", &T);
		check("(false || true)", &T);
		check("(false || false)", &F);
		check("(true || false)", &T);
		check("(true || true)", &T);
	}

	#[test]
	fn modal_operators() {
		check("[tau]true", &T);
		check("<tau>false", &F);
		snapshot!(eval("<tau>[tau]true").0);
		snapshot!(eval("[tau]false").0);
		snapshot!(eval("<tau>[tau]false").0);
	}

	#[test]
	fn fixpoints() {
		check("nu X. X", &T);
		check("mu Y. Y", &F);
		snapshot!(eval("nu X. mu Y. (X || Y)"));
		snapshot!(eval("nu X. mu Y. (X && Y)"));
		snapshot!(eval("nu X. (X && mu Y. Y)"));
	}

	#[test]
	fn combined() {
		check("nu X. mu Y. ( (<tau>Y || <a>Y) || <b>X)", &T);
		// all except 3, 5, 7
		snapshot!(eval("nu X. (<tau>X && mu Y. (<tau>Y || [a]false))"));
		snapshot!(eval("nu X. <tau>X")); // all except 3, 5, 7
		snapshot!(eval("nu X. mu Y. ( <tau>Y || <a>X)")); // all except 7
		snapshot!(eval("mu X. ([tau]X && (<tau>true || <a>true))")); // only 3, 5
	}
}

mod dining {
	use super::*;

	const PROPS: [(&str, &str); 4] = [
		(
			"IPE",
			include_str!("../data/dining/invariantly_possibly_eat.mcf"),
		),
		(
			"IIE",
			include_str!("../data/dining/invariantly_inevitably_eat.mcf"),
		),
		(
			"IPS",
			include_str!("../data/dining/invariantly_possibly_eat.mcf"),
		),
		(
			"PIE",
			include_str!("../data/dining/plato_infinitely_often_can_eat.mcf"),
		),
	];

	fn eval(lts: &str, formula: &str) -> (StateSet, u32) {
		init();
		let lts = lts::from_aldebaran(lts).unwrap();
		let f = formula.parse::<myu::calculus::Formula>().unwrap();
		myu::calculus::emerson_lei(&lts, &f)
	}

	#[test]
	fn d3() {
		let lts = include_str!("../data/dining/dining_3.aut");
		for (prop, formula) in PROPS {
			set_snapshot_suffix!("-{}", prop);
			snapshot!(eval(lts, formula))
		}
	}

	#[test]
	fn d5() {
		let lts = include_str!("../data/dining/dining_5.aut");
		for (prop, formula) in PROPS {
			set_snapshot_suffix!("-{}", prop);
			snapshot!(eval(lts, formula))
		}
	}

	#[test]
	fn d7() {
		let lts = include_str!("../data/dining/dining_7.aut");
		for (prop, formula) in PROPS {
			set_snapshot_suffix!("-{}", prop);
			snapshot!(eval(lts, formula))
		}
	}

	#[test]
	fn d9() {
		let lts = include_str!("../data/dining/dining_9.aut");
		for (prop, formula) in PROPS {
			set_snapshot_suffix!("-{}", prop);
			snapshot!(eval(lts, formula))
		}
	}
}
