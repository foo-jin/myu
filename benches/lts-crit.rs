use criterion::{criterion_group, criterion_main, BenchmarkId, Criterion};
use myu::lts;
use std::str::FromStr;

fn bench(c: &mut Criterion, gname: &str, lts: &str, props: &[(&str, &str)]) {
	let mut group = c.benchmark_group("lts::em_lei");
	let lts = lts::from_aldebaran(lts).unwrap();
	for (name, f) in props.iter() {
		let f = myu::Formula::from_str(f).unwrap();
		group.bench_with_input(BenchmarkId::new(gname, name), &f, |b, f| {
			b.iter(|| myu::calculus::emerson_lei(&lts, f));
		});
	}
	group.finish();
}

fn dining(c: &mut Criterion) {
	const LTS: &str = include_str!("../data/dining/dining_4.aut");
	const PROPS: [(&str, &str); 4] = [
		// dining philosophers
		(
			"IPE",
			include_str!("../data/dining/invariantly_possibly_eat.mcf"),
		),
		(
			"IIE",
			include_str!("../data/dining/invariantly_inevitably_eat.mcf"),
		),
		(
			"IPS",
			include_str!("../data/dining/invariantly_possibly_eat.mcf"),
		),
		(
			"PIE",
			include_str!("../data/dining/plato_infinitely_often_can_eat.mcf"),
		),
	];

	bench(c, "dining", LTS, &PROPS);
}

fn demanding(c: &mut Criterion) {
	const LTS: &str = include_str!("../data/demanding/demanding_children_4.aut");
	const PROPS: [(&str, &str); 2] = [
		// demanding children
		(
			"IPA",
			include_str!("../data/demanding/invariantly_possibly_ask.mcf"),
		),
		(
			"NABQ",
			include_str!("../data/demanding/no_answer_before_question.mcf"),
		),
	];

	bench(c, "demanding", LTS, &PROPS);
}

fn ccp(c: &mut Criterion) {
	const LTS: &str = include_str!("../data/ccp/german_linear_2.1.aut");
	const PROPS: [(&str, &str); 5] = [
		// cache coherence protocol
		(
			"IRNA",
			include_str!("../data/ccp/infinite_run_no_access.mcf"),
		),
		(
			"IOE",
			include_str!("../data/ccp/infinitely_often_exclusive.mcf"),
		),
		(
			"IEFSA",
			include_str!("../data/ccp/invariantly_eventually_fair_shared_access.mcf"),
		),
		(
			"IIEA",
			include_str!("../data/ccp/invariantly_inevitably_exclusive_access.mcf"),
		),
		(
			"IPEA",
			include_str!("../data/ccp/invariantly_possibly_exclusive_access.mcf"),
		),
	];

	bench(c, "ccp", LTS, &PROPS);
}

fn boardgame(c: &mut Criterion) {
	const LTS: &str = include_str!("../data/boardgame/robots_50.aut");
	const PROPS: [(&str, &str); 3] = [
		// boardgame
		("PW", include_str!("../data/boardgame/possibly_win.mcf")),
		("CW", include_str!("../data/boardgame/certainly_win.mcf")),
		("IW", include_str!("../data/boardgame/infinitely_win.mcf")),
	];

	bench(c, "boardgame", LTS, &PROPS);
}

criterion_group! {
	name = lts;
	config = Criterion::default().noise_threshold(0.05);
	targets = dining, demanding, ccp, boardgame
}

criterion_main!(lts);
