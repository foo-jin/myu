use myu::{bes::Bes, lts, Formula, StateSet};
use std::str::FromStr;

const DINING_2: &str = include_str!("../data/dining/dining_2.aut");

const PROPS: [(&str, &str); 4] = [
	(
		"IPE",
		include_str!("../data/dining/invariantly_possibly_eat.mcf"),
	),
	(
		"IIE",
		include_str!("../data/dining/invariantly_inevitably_eat.mcf"),
	),
	(
		"IPS",
		include_str!("../data/dining/invariantly_possibly_eat.mcf"),
	),
	(
		"PIE",
		include_str!("../data/dining/plato_infinitely_often_can_eat.mcf"),
	),
];

fn run_bench(lts: &str, f: &str) -> StateSet {
	let lts = lts::from_aldebaran(lts).unwrap();
	let f = Formula::from_str(f).unwrap();
	let bes = Bes::try_from((&lts, &f)).unwrap();
	bes.eval()
}

fn bes_ipe() -> StateSet {
	run_bench(DINING_2, PROPS[0].1)
}

fn bes_iie() -> StateSet {
	run_bench(DINING_2, PROPS[1].1)
}

fn bes_ips() -> StateSet {
	run_bench(DINING_2, PROPS[2].1)
}

fn bes_pie() -> StateSet {
	run_bench(DINING_2, PROPS[3].1)
}

iai::main!(bes_ipe, bes_iie, bes_ips, bes_pie);
