use criterion::{criterion_group, criterion_main, Criterion};
use myu::{bes::Bes, lts};
use std::str::FromStr;

const DINING_2: &str = include_str!("../data/dining/dining_2.aut");

const PROPS: [(&str, &str); 4] = [
	(
		"IPE",
		include_str!("../data/dining/invariantly_possibly_eat.mcf"),
	),
	(
		"IIE",
		include_str!("../data/dining/invariantly_inevitably_eat.mcf"),
	),
	(
		"IPS",
		include_str!("../data/dining/invariantly_possibly_eat.mcf"),
	),
	(
		"PIE",
		include_str!("../data/dining/plato_infinitely_often_can_eat.mcf"),
	),
];

fn short(c: &mut Criterion) {
	let mut group = c.benchmark_group("myu::bes::gauss_elim");
	let lts = lts::from_aldebaran(DINING_2).unwrap();
	for (name, f) in PROPS.iter() {
		let f = myu::Formula::from_str(f).unwrap();
		let bes = Bes::try_from((&lts, &f)).unwrap();
		group.bench_with_input(*name, &bes, |b, bes| b.iter(|| bes.clone().eval()));
	}
	group.finish();
}

criterion_group! {
	name = bes;
	config = Criterion::default().noise_threshold(0.05);
	targets = short
}

criterion_main!(bes);
