use myu::{lts, Formula};
use std::str::FromStr;

fn run_bench(lts: &str, f: &str) {
	let lts = lts::from_aldebaran(lts).unwrap();
	let f = Formula::from_str(f).unwrap();
	myu::calculus::emerson_lei(&lts, &f);
}

fn lts_dining() {
	const LTS: &str = include_str!("../data/dining/dining_6.aut");
	const PROPS: [(&str, &str); 4] = [
		// dining philosophers
		(
			"IPE",
			include_str!("../data/dining/invariantly_possibly_eat.mcf"),
		),
		(
			"IIE",
			include_str!("../data/dining/invariantly_inevitably_eat.mcf"),
		),
		(
			"IPS",
			include_str!("../data/dining/invariantly_possibly_eat.mcf"),
		),
		(
			"PIE",
			include_str!("../data/dining/plato_infinitely_often_can_eat.mcf"),
		),
	];

	for (_, p) in PROPS {
		run_bench(LTS, p);
	}
}

fn lts_demanding() {
	const LTS: &str = include_str!("../data/demanding/demanding_children_4.aut");
	const PROPS: [(&str, &str); 2] = [
		// demanding children
		(
			"IPA",
			include_str!("../data/demanding/invariantly_possibly_ask.mcf"),
		),
		(
			"NABQ",
			include_str!("../data/demanding/no_answer_before_question.mcf"),
		),
	];

	for (_, p) in PROPS {
		run_bench(LTS, p);
	}
}

fn lts_ccp() {
	const LTS: &str = include_str!("../data/ccp/german_linear_2.1.aut");
	const PROPS: [(&str, &str); 5] = [
		// cache coherence protocol
		(
			"IRNA",
			include_str!("../data/ccp/infinite_run_no_access.mcf"),
		),
		(
			"IOE",
			include_str!("../data/ccp/infinitely_often_exclusive.mcf"),
		),
		(
			"IEFSA",
			include_str!("../data/ccp/invariantly_eventually_fair_shared_access.mcf"),
		),
		(
			"IIEA",
			include_str!("../data/ccp/invariantly_inevitably_exclusive_access.mcf"),
		),
		(
			"IPEA",
			include_str!("../data/ccp/invariantly_possibly_exclusive_access.mcf"),
		),
	];

	for (_, p) in PROPS {
		run_bench(LTS, p);
	}
}

fn lts_boardgame() {
	const LTS: &str = include_str!("../data/boardgame/robots_50.aut");
	const PROPS: [(&str, &str); 3] = [
		// boardgame
		("PW", include_str!("../data/boardgame/possibly_win.mcf")),
		("CW", include_str!("../data/boardgame/certainly_win.mcf")),
		("IW", include_str!("../data/boardgame/infinitely_win.mcf")),
	];

	for (_, p) in PROPS {
		run_bench(LTS, p);
	}
}

iai::main!(lts_dining, lts_demanding, lts_ccp, lts_boardgame);
